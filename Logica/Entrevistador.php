<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/EntrevistadorDAO.php";
class Entrevistador{
    private $idEntrevistador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    private $conexion;
    private $entrevistadorDAO;
 
    public function getIdEntrevistador(){
        return $this -> idEntrevistador;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }

    public function getEstado(){
        return $this -> estado;
    }

    public function Entrevistador($idEntrevistador = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "",$estado= ""){
        $this -> idEntrevistador = $idEntrevistador;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> entrevistadorDAO = new EntrevistadorDAO($this -> idEntrevistador, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> estado);
    }

    public function BuscarLog(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> entrevistadorDAO -> BuscarLog());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idAdministrador = $resultado[0];
            $this -> entrevistadorDAO = new EntrevistadorDAO($this -> idEntrevistador);
            return true;
        }else {
            return false;
        }
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> entrevistadorDAO -> autenticar());
        $this -> conexion -> cerrar();       
        if ($this -> conexion -> numFilas() == 1){            
            $resultado = $this -> conexion -> extraer();
            $this -> idEntrevistador = $resultado[0];
            $this -> estado = $resultado[1];             
            return true;        
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> entrevistadorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
        $this -> clave = $resultado[4];
    }

     // para uso de ajax tabla
     public function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultarTodos());
        $administrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Administrador($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($administrador, $c);
        }
        $this -> conexion -> cerrar();
        return $administrador;
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> entrevistadorDAO -> consultarPaginacion($cantidad, $pagina));
        $aspirantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Entrevistador($resultado[0], $resultado[1], $resultado[2], $resultado[3],"", $resultado[4], $resultado[5]);
            array_push($aspirantes, $p);
        }
        $this -> conexion -> cerrar();
        return $aspirantes;
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> entrevistadorDAO -> consultarPaginacionFiltro($cantidad, $pagina, $filtro));
        $administrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Entrevistador($resultado[0], $resultado[1], $resultado[2], $resultado[3],"",$resultado[4],$resultado[5]);
            array_push($administrador, $p);
        }
        $this -> conexion -> cerrar();
        return $administrador;
    }

    public function consultarCantidadFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultarCantidadFiltro($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }    

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

// para uso de ajax editar
    public function editarColumn($column,$editableObj,$id){ 
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> editarColumn($column,$editableObj,$id));
        $this -> conexion -> cerrar();
    }

    public function existeCorreo(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> administradorDAO -> existeCorreo());        
        $this -> conexion -> cerrar();        
        return $this -> conexion -> numFilas();
    }

    public function editar($param,$edicion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> entrevistadorDAO -> editar($param,$edicion));
        $this -> conexion -> cerrar();
    }

    public function editarClave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> editarClave());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodosId(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> entrevistadorDAO -> consultarTodosId());
        $aspirantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Recursos($resultado[0]);
            array_push($aspirantes, $p);
        }
        $this -> conexion -> cerrar();
        return $aspirantes;
    }
    
    public function editarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> entrevistadorDAO -> editarFoto());
        $this -> conexion -> cerrar();
    }
}

?>