<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CargoAspiranteDAO.php";
class CargoAspirante{
    private $idCargo;
    private $idAspirante;
    private $cargoAspitanteDAO;

    public function geIdCargo(){
        return $this -> idCargo;
    }

    public function getIdAspirante(){
        return $this -> idAspirante;
    }

    public function CargoAspirante($idCargo = "", $idAspirante = ""){
        $this -> idCargo = $idCargo;
        $this -> idAspirante = $idAspirante;
        $this -> conexion = new Conexion();
        $this -> cargoAspitanteDAO = new CargoAspiranteDAO($this -> idCargo, $this -> idAspirante);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> cargoAspitanteDAO -> insertar());    
        $this -> conexion -> cerrar();        
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoAspitanteDAO -> consultarTodos());
        $administrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new CargoAspirante($resultado[0], $resultado[1]);
            array_push($administrador, $c);
        }
        $this -> conexion -> cerrar();
        return $administrador;
    }

    public function consultarTodosA(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoAspitanteDAO -> consultarTodosA());
        $administrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new CargoAspirante($resultado[0], $resultado[1]);
            array_push($administrador, $c);
        }
        $this -> conexion -> cerrar();
        return $administrador;
    }

}
