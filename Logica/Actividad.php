<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ActividadDAO.php";

class Actividad{
    private $idActividad;
    private $fecha;
    private $descripcion;
    private $idContratacion;
    private $idActor;
    private $actividadDAO;
    private $tipoActor;
    private $conexion;

    public function getIdActividad(){
        return $this -> idActividad;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getIdContratacion(){
        return $this -> idContratacion;
    }

    public function getFecha(){
        return $this -> fecha;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function getIdActor(){
        return $this -> idActor;
    }

    public function getTipoActor(){
        return $this -> tipoActor;
    }

    public function Actividad($idActividad = "", $fecha = "", $descripcion = "", $idContratacion = "", $idActor = "", $tipoActor = ""){
        $this -> idActividad = $idActividad;
        $this -> fecha = $fecha;
        $this -> descripcion = $descripcion;
        $this -> idContratacion = $idContratacion;
        $this -> idActor = $idActor;
        $this -> tipoActor = $tipoActor;
        $this -> conexion = new Conexion();
        $this -> actividadDAO = new ActividadDAO($this -> idActividad, $this -> fecha, $this -> descripcion, $this -> idContratacion, $this -> idActor, $this -> tipoActor);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> actividadDAO -> insertar());    
        $this -> conexion -> cerrar();        
    }

    public function consultarJefe(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCant(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacion($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacionJefe($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionJefe($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> actividadDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idActividad = $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> descripcion = $resultado[2];
        $this -> idContratacion = $resultado[3];
        $this -> idActor = $resultado[4];
        $this -> tipoActor= $resultado[5];
    }

     // para uso de ajax tabla
     public function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> actividadDAO -> consultarTodos());
        $actividad = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Actividad($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
            array_push($actividad, $c);
        }
        $this -> conexion -> cerrar();
        return $actividad;
    }

    public function consultarCantidadFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltro($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltro($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
    
    public function consultarCantidadFiltroJ($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltroJ($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultarPaginacionFiltroJ($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltroJ($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
}
