<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CargoDAO.php";
class Cargo{
    private $idCargo;
    private $nombre;
    private $vacantes;
    private $fecha;
    private $idJefe;
    private $estado;
    private $descripcion;
    private $cargoDAO;

    public function geIdCargo(){
        return $this -> idCargo;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getVacantes(){
        return $this -> vacantes;
    }

    public function getFecha(){
        return $this -> fecha;
    }

    public function getIdJefe(){
        return $this -> idJefe;
    }

    public function getEstadp(){
        return $this -> estado;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function Cargo($idCargo = "", $nombre = "", $vacantes = "", $fecha = "", $idJefe = "", $estado = "", $descripcion = ""){
        $this -> idCargo = $idCargo;
        $this -> nombre = $nombre;
        $this -> vacantes = $vacantes;
        $this -> fecha = $fecha;
        $this -> idJefe = $idJefe;
        $this -> estado = $estado;
        $this -> descripcion = $descripcion;
        $this -> conexion = new Conexion();
        $this -> cargoDAO = new CargoDAO($this -> idCargo, $this -> nombre, $this -> vacantes, $this -> fecha, $this -> idJefe, $this -> estado, $this -> descripcion);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> cargoDAO -> insertar());    
        $this -> conexion -> cerrar();        
    }

    public function consultarJefe(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCant(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacion($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacionJefe($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionJefe($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacionRec($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionRec($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacionFiltroRec($cantidad, $pagina, $filtro,$idRec){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltroRec($cantidad, $pagina, $filtro,$idRec));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }

    public function consultarCantidadFiltroRec($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltroRec($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idCargo = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> vacantes = $resultado[2];
        $this -> fecha = $resultado[3];
        $this -> estado = $resultado[4];
        $this -> descripcion = $resultado[5];
        $this -> idJefe = $resultado[6];
    }

     // para uso de ajax tabla
     public function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarTodos());
        $administrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Administrador($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($administrador, $c);
        }
        $this -> conexion -> cerrar();
        return $administrador;
    }

    public function consultarCantidadFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltro($filtro));
        $this -> conexion -> cerrar();        
        $cont = $this -> conexion -> extraer()[0];   
        return $cont;
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltro($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
    
    public function consultarCantidadFiltroJ($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltroJ($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultarPaginacionFiltroJ($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltroJ($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
    public function editar($param,$edicion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> editar($param,$edicion));
        $this -> conexion -> cerrar();
    }
}
