<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogDAO.php";

class Log{
    private $idLog;
    private $fecha;
    private $hora;
    private $datos;
    private $idLogAccion;
    private $nombre;
    private $actor;
    private $conexion;
    private $logDAO;

    public function getIdLog(){
        return $this -> idLog;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getHora(){
        return $this -> hora;
    }

    public function getFecha(){
        return $this -> fecha;
    }

    public function getDatos()
    {
        return $this-> datos;
    }

    public function getIdLogAccion(){
        return $this -> idLogAccion;
    }

    public function getActor(){
        return $this -> actor;
    }

    public function Log($idLog = "", $fecha = "", $hora = "", $datos = "", $idLogAccion = "", $nombre = "",$actor=""){
        $this -> idLog = $idLog;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> datos = $datos;
        $this -> idLogAccion = $idLogAccion;
        $this -> nombre = $nombre;
        $this -> actor = $actor;
        $this -> conexion = new Conexion();
        $this -> logDAO = new LogDAO($this -> idLog, $this -> fecha, $this -> hora, $this -> datos, $this -> idLogAccion, $this -> nombre, $this -> actor);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> logDAO -> insertar());    
        $this -> conexion -> cerrar();        
    }

    public function asociarA($id){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> logDAO -> asociarA($id));    
        $this -> conexion -> cerrar();        
    }

    public function asociarJ($id){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> logDAO -> asociarJ($id));    
        $this -> conexion -> cerrar();        
    }

    public function asociarR($id){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> logDAO -> asociarR($id));    
        $this -> conexion -> cerrar();        
    }

    public function asociarE($id){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> logDAO -> asociarE($id));    
        $this -> conexion -> cerrar();        
    }

    public function asociarAd($id){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> logDAO -> asociarAd($id));    
        $this -> conexion -> cerrar();        
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idLog = $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> hora = $resultado[2];
        $this -> datos = $resultado[3];
        $this -> idLogAccion = $resultado[4];
        $this -> nombre = $resultado[5];
        $this -> actor = $resultado[6];
    }

    public function consultarPaginacionAspF($cantidad, $pagina,$filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarPaginacionAspF($cantidad, $pagina,$filtro));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Log($resultado[0],$resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5],$resultado[6]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCantidadAspF($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidadAspF($filtro));
        $cont = $this -> conexion -> extraer()[0];
        return $cont;
    }

    public function consultarJ(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarJ());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idLog = $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> hora = $resultado[2];
        $this -> datos = $resultado[3];
        $this -> idLogAccion = $resultado[4];
        $this -> nombre = $resultado[5];
        $this -> actor = $resultado[6];
    }

    public function consultarPaginacionJefF($cantidad, $pagina,$filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarPaginacionJefF($cantidad, $pagina,$filtro));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Log($resultado[0],$resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5],$resultado[6]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCantidadJefF($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidadJefF($filtro));
        $cont = $this -> conexion -> extraer()[0];
        return $cont;
    }

    public function consultarR(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarR());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idLog = $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> hora = $resultado[2];
        $this -> datos = $resultado[3];
        $this -> idLogAccion = $resultado[4];
        $this -> nombre = $resultado[5];
        $this -> actor = $resultado[6];
    }

    public function consultarPaginacionRecF($cantidad, $pagina,$filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarPaginacionRecF($cantidad, $pagina,$filtro));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Log($resultado[0],$resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5],$resultado[6]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCantidadRecF($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidadRecF($filtro));
        $cont = $this -> conexion -> extraer()[0];
        return $cont;
    }

    public function consultarE(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarE());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idLog = $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> hora = $resultado[2];
        $this -> datos = $resultado[3];
        $this -> idLogAccion = $resultado[4];
        $this -> nombre = $resultado[5];
        $this -> actor = $resultado[6];
    }

    public function consultarPaginacionEF($cantidad, $pagina,$filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarPaginacionEF($cantidad, $pagina,$filtro));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Log($resultado[0],$resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5],$resultado[6]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCantidadEF($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidadEF($filtro));
        $cont = $this -> conexion -> extraer()[0];
        return $cont;
    }

    public function consultarAd(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarAd());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idLog = $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> hora = $resultado[2];
        $this -> datos = $resultado[3];
        $this -> idLogAccion = $resultado[4];
        $this -> nombre = $resultado[5];
        $this -> actor = $resultado[6];
    }

    public function consultarPaginacionAdF($cantidad, $pagina,$filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarPaginacionAdF($cantidad, $pagina,$filtro));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Log($resultado[0],$resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5],$resultado[6]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCantidadAdF($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidadAdF($filtro));
        $cont = $this -> conexion -> extraer()[0];
        return $cont;
    }

}
