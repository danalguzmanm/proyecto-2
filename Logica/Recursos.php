<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/RecursosDAO.php";
class Recursos{
    private $idRecursos;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;      
    private $estado;
    private $conexion;
    private $RecursosDAO;

    public function getIdRecursos(){
        return $this -> idRecursos;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Recursos($idRecursos = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "",$estado = ""){
        $this -> idRecursos = $idRecursos;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> RecursosDAO = new RecursosDAO($this -> idRecursos, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> estado);
    } 
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> RecursosDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idRecursos = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> RecursosDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
    }
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> RecursosDAO -> consultarPaginacion($cantidad, $pagina));
        $aspirantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Recursos($resultado[0], $resultado[1], $resultado[2], $resultado[3],"", $resultado[4], $resultado[5]);
            array_push($aspirantes, $p);
        }
        $this -> conexion -> cerrar();
        return $aspirantes;
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> RecursosDAO -> consultarPaginacionFiltro($cantidad, $pagina, $filtro));
        $administrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Recursos($resultado[0], $resultado[1], $resultado[2], $resultado[3],"",$resultado[4],$resultado[5]);
            array_push($administrador, $p);
        }
        $this -> conexion -> cerrar();
        return $administrador;
    }

    public function consultarTodosId(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> RecursosDAO -> consultarTodosId());
        $aspirantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Recursos($resultado[0]);
            array_push($aspirantes, $p);
        }
        $this -> conexion -> cerrar();
        return $aspirantes;
    }

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> RecursosDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function editar($param,$edicion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> RecursosDAO -> editar($param,$edicion));
        $this -> conexion -> cerrar();
    }

    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> RecursosDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function editarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> RecursosDAO -> editarFoto());
        $this -> conexion -> cerrar();
    }
    
}
