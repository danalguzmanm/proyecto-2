<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/AspiranteDAO.php";
class Aspirante{
    private $idAspirante;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $edad;
    private $sexo;
    private $civil;
    private $descripcion;
    private $foto;     
    private $estado; 
    private $conexion;
    private $AspiranteDAO;

    public function getIdAspirante(){
        return $this -> idAspirante;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }
    public function getEdad(){
        return $this -> edad;
    }
    public function getSexo(){
        return $this -> sexo;
    }
    public function geTCivil(){
        return $this -> civil;
    }
    public function getDescripcion(){
        return $this -> descripcion;
    }
    
    public function getEstado(){
        return $this -> estado;
    }

    public function Aspirante($idAspirante = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $edad = "", $sexo = "", $civil = "", $descripcion = "",$estado = ""){
        $this -> idAspirante = $idAspirante;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> edad = $edad;
        $this -> sexo = $sexo;
        $this -> civil = $civil;
        $this -> estado = $estado;
        $this -> descripcion = $descripcion;
        $this -> conexion = new Conexion();
        $this -> AspiranteDAO = new AspiranteDAO($this -> idAspirante, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> edad, $this -> sexo, $this -> civil, $this -> descripcion, $this -> estado);
    } 
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idAspirante = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
     
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
        $this -> idAspirante = $resultado[4];
        $this -> edad = $resultado[5];
        $this -> sexo = $resultado[6];
        $this -> civil = $resultado[7];
        $this -> descripcion = $resultado[8];
        $this -> estado = $resultado[9];
    }

    public function editar($param,$edicion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> editar($param,$edicion));
        $this -> conexion -> cerrar();
    }

    public function insertarFactBasic($cargo,$edad,$exp,$sexo,$civil){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> insertarFactBasic($cargo,$edad,$exp,$sexo,$civil));    
        $this -> conexion -> cerrar();        
    }

    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    public function editarA(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> editarA());
        $this -> conexion -> cerrar();
    }
    public function editarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> editarFoto());
        $this -> conexion -> cerrar();
    }
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> consultarPaginacion($cantidad, $pagina));
        $aspirantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Aspirante($resultado[0], $resultado[1], $resultado[2],$resultado[3],"", $resultado[4],"","","","",$resultado[5]);
            array_push($aspirantes, $p);
        }
        $this -> conexion -> cerrar();
        return $aspirantes;
    } 

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> consultarPaginacionFiltro($cantidad, $pagina,$filtro));
        $aspirantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Aspirante($resultado[0], $resultado[1], $resultado[2],$resultado[3],"", $resultado[4],"","","","",$resultado[5]);
            array_push($aspirantes, $p);
        }
        $this -> conexion -> cerrar();
        return $aspirantes;
    }

    public function consultarPaginacionRec($cantidad, $pagina,$idCargo){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> consultarPaginacionRec($cantidad, $pagina,$idCargo));
        $aspirantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Aspirante($resultado[0], $resultado[1], $resultado[2],$resultado[3],"",$resultado[4],$resultado[5],$resultado[6],$resultado[7],$resultado[8]);
            array_push($aspirantes, $p);
        }
        $this -> conexion -> cerrar();
        return $aspirantes;
    }

    public function existeCorreo(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> existeCorreo());        
        $this -> conexion -> cerrar();        
        return $this -> conexion -> numFilas();
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> registrar());
        $this -> conexion -> cerrar();         
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> AspiranteDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
}

?>