<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/EncuestaDAO.php";
class Encuesta{
    private $idEncuesta;
    private $idAspirante;
    private $tipo;
    private $puntaje;
    private $encuestaDAO;
    private $idEntrevistador;
 
    public function getIdEncuesta(){
        return $this -> idEncuesta;
    }

    public function getIdAspirante(){
        return $this -> idAspirante;
    }

    public function getTipo(){
        return $this -> tipo;
    }

    public function getPuntaje(){
        return $this -> puntaje;
    }

    public function getIdEntrevistador(){
        return $this -> idEntrevistador;
    }

    public function Encuesta($idEncuesta = "", $idAspirante = "", $tipo = "", $puntaje = "",$idEntrevistador=""){
        $this -> idEncuesta = $idEncuesta;
        $this -> idAspirante = $idAspirante;
        $this -> tipo = $tipo;
        $this -> puntaje = $puntaje;
        $this -> idEntrevistador = $idEntrevistador;
        $this -> conexion = new Conexion(); 
        $this -> encuestaDAO = new EncuestaDAO($this -> idEncuesta, $this -> idAspirante, $this -> tipo, $this -> puntaje,$this -> idEntrevistador);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> encuestaDAO -> insertar());    
        $this -> conexion -> cerrar();        
    }

    public function eliminar(){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> encuestaDAO -> eliminar());    
        $this -> conexion -> cerrar();        
    }

    public function consultarE(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encuestaDAO -> consultarE());
        $this -> conexion -> cerrar();        
        $cont = $this -> conexion -> extraer()[0];
        return $cont;
    }

    public function consultarPunt(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> encuestaDAO -> consultarPunt());
        $aspirantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Encuesta($resultado[0],$resultado[1],$resultado[2],$resultado[3],$resultado[4]);
            array_push($aspirantes, $p);
        }
        $this -> conexion -> cerrar();
        return $aspirantes;
    }
    public function consultarPerson($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encuestaDAO -> consultarPerson($id));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Encuesta($resultado[0], $resultado[3], $resultado[1], $resultado[2] ,$resultado[4]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }
    public function consultarOtra($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encuestaDAO -> consultarOtra($id));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Encuesta($resultado[0], $resultado[3], $resultado[1], $resultado[2] ,$resultado[4]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }
    public function consultarEspa($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encuestaDAO -> consultarEspa($id));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Encuesta($resultado[0], $resultado[3], $resultado[1], $resultado[2] ,$resultado[4]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarEn($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> encuestaDAO -> consultarEn($id));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Encuesta($resultado[0], $resultado[3], $resultado[1], $resultado[2] ,$resultado[4]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }
}

?>