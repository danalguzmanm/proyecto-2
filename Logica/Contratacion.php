<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ContratacionDAO.php";
class Contratacion{
    private $idContratacion;
    private $idRecursos;
    private $idCargo;
    private $conexion;
    private $contratacionDAO;

    public function geIdCargo(){
        return $this -> idCargo;
    }

    public function getIdContratacion(){
        return $this -> idContratacion;
    }

    public function getIdRecursos(){
        return $this -> idRecursos;
    }

    public function Contratacion($idContratacion = "", $idRecursos = "", $idCargo = ""){
        $this -> idContratacion = $idContratacion;
        $this -> idRecursos = $idRecursos;
        $this -> idCargo = $idCargo;
        $this -> conexion = new Conexion();
        $this -> contratacionDAO = new ContratacionDAO($this -> idContratacion, $this -> idRecursos, $this -> idCargo);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();    
        $this -> conexion -> ejecutar($this -> contratacionDAO -> insertar());    
        $this -> conexion -> cerrar();        
    }

    public function consultarJefe(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCant(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacion($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Contratacion($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacionRec($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> contratacionDAO -> consultarPaginacionRec($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Contratacion($resultado[0],$resultado[1], $resultado[2]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacionJefe($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionJefe($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    } 

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratacionDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idContratacion = $resultado[0];
        $this -> idRecursos = $resultado[1];
        $this -> idCargo = $resultado[2];
    } 

    public function consultarRec(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratacionDAO -> consultarRec());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idContratacion = $resultado[0];
        $this -> idRecursos = $resultado[1];
        $this -> idCargo = $resultado[2];
    }

    public function consultarRec2(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratacionDAO -> consultarRec2());
        $this -> conexion -> cerrar();
        $administrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Contratacion($resultado[0], $resultado[1], $resultado[2]);
            array_push($administrador, $c);
        }
        return $administrador;
    }

     // para uso de ajax tabla
     public function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarTodos());
        $administrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Administrador($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($administrador, $c);
        }
        $this -> conexion -> cerrar();
        return $administrador;
    }

    public function consultarCantidadFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltro($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltro($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
    
    public function consultarCantidadFiltroJ($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltroJ($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultarPaginacionFiltroJ($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltroJ($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
}
