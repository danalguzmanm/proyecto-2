<?php
session_start();
require_once "logica/Administrador.php";
require_once "logica/Entrevistador.php";
require_once "logica/Jefe.php";
require_once "logica/Cargo.php";
require_once "Logica/Aspirante.php";
require_once "Logica/Encuesta.php";
require_once "Logica/Recursos.php";
require_once "Logica/Contratacion.php";
require_once "Logica/Actividad.php";
require_once "Logica/CargoAspirante.php";
require_once "Logica/Log.php";
require_once "Logica/Encuesta.php";
$pid = "";
if (isset($_GET["pid"])) {
	$pid = base64_decode($_GET["pid"]);
} else {
	$_SESSION["id"] = "";
	$_SESSION["rol"] = "";
}
if (isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])) {
	$_SESSION["id"] = "";
}
?>
<html>

<head>
	<title>Punto&Coma</title>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<link rel="stylesheet" href="css/estilos.css" />
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.js"></script>
</head>

<body>
	<?php
	include "presentacion/encabezado.php";
	$paginasSinSesion = array(
		"presentacion/autenticar.php",
		"presentacion/aspirante/registrarAspirante.php",
		"presentacion/inicioPc.php",
	);
	if (in_array($pid, $paginasSinSesion)) {
		include $pid;
	} else if ($_SESSION["id"] != "") {
		if ($_SESSION["rol"] == "Administrador") {
			include "presentacion/administrador/menuAdministrador.php";
		} else if ($_SESSION["rol"] == "Entrevistador") {
			include "presentacion/entrevistador/menuEntrevistador.php";
		} else if ($_SESSION["rol"] == "Jefe") {
			include "presentacion/jefe/menuJefe.php";
		}else if($_SESSION["rol"] == "Aspirante"){
			include "Presentacion/Aspirante/menuAspirante.php";
		}else if($_SESSION["rol"] == "Recursos"){
			include "Presentacion/recursos/menuRecursos.php";
		}
		include $pid;
	} else {
		include "presentacion/menuInicio.php";
		include "presentacion/inicio.php";
	}
	?>
</body>

</html>