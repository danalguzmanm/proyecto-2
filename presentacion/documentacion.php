<?php
require('fpdf/fpdf.php');
ob_end_clean(); //    the buffer and never prints or returns anything.
ob_start(); // it starts buffering

$idCargo = "";
if (isset($_GET["id"])) {
    $idCargo = $_GET["id"];
}

$cargo = new Cargo($idCargo);
$cargo->consultar();

$contratacion = new Contratacion("", "", $cargo->geIdCargo());
$contratacion->consultar();

$actividad = new Actividad("", "", "", $contratacion->getIdContratacion());
$actividades = $actividad->consultarTodos();

$jefe = new Jefe($cargo->getIdJefe());
$jefe->consultar();

$recursos = new Recursos($contratacion->getIdRecursos());
$recursos->consultar();

$casp = new CargoAspirante($cargo->geIdCargo());
$casps = $casp->consultarTodos();

//Titulo y logo pdf
$pdf = new FPDF("P", "mm", "Letter");
$pdf->SetFont("Times", "I", 25);
$pdf->AddPage();
$pdf->SetXY(0, 0);
$pdf->Image('img/logo.png', 15, 5, 22);
$pdf->Cell(216, 40,  utf8_decode("Documentación cargo " . $cargo->getNombre()), 0, 2, "C");


$pdf->SetFont("Arial", "B", 13);
$pdf->SetDrawColor(46, 200, 240);
$pdf->SetLineWidth(2);
$pdf->Line(10, 30, 195, 30);
$pdf->Ln(1);

//Informacion base sobre el cargo

$pdf->SetDrawColor(250, 250, 250);
$pdf->SetLineWidth(1);

$pdf->Cell(35, 10, utf8_decode("Código:"), 'T', 0, 'L');
$pdf->SetFont("Arial", "", 13);
$pdf->Cell(20, 10, utf8_decode($cargo->geIdCargo()), 0, 0, 'L');
$pdf->Ln();

$pdf->SetFont("Arial", "B", 13);
$pdf->Cell(35, 10, utf8_decode("Vacantes:"), 0, 0, 'L');
$pdf->SetFont("Arial", "", 13);
$pdf->Cell(20, 10, utf8_decode($cargo->getVacantes()), 0, 0, 'L');
$pdf->Ln();

$pdf->SetFont("Arial", "B", 13);
$pdf->Cell(35, 10, utf8_decode("Fecha:"), 0, 0, 'L');
$pdf->SetFont("Arial", "", 13);
$pdf->Cell(20, 10, utf8_decode($cargo->getFecha()), 0, 0, 'L');
$pdf->Ln();

$pdf->SetFont("Arial", "B", 13);
$pdf->Cell(35, 10, utf8_decode("Descripción:"), 0, 0, 'L');
$pdf->SetFont("Arial", "", 13);
$pdf->MultiCell(150, 6, utf8_decode($cargo->getDescripcion()), 0, 'L');
$pdf->Ln();

$pdf->SetFont("Arial", "B", 13);
$pdf->Cell(35, 10, utf8_decode("Jefe solicitante: "), 0, 0, 'L');
$pdf->SetFont("Arial", "", 13);
$pdf->Cell(20, 10, utf8_decode($jefe->getNombre() . " " . $jefe->getApellido()), 0, 0, 'L');
$pdf->Ln();

$pdf->SetFont("Arial", "B", 13);
$pdf->Cell(35, 10, utf8_decode("Encargado RH: "), 0, 0, 'L');
$pdf->SetFont("Arial", "", 13);
$pdf->Cell(20, 10, utf8_decode($recursos->getNombre() . " " . $recursos->getApellido()), 0, 0, 'L');
$pdf->Ln();

//Informacion sobre encargados



//imagen en celda

/* $pdf->SetFont("Arial", "B", 13);
$pdf->Cell(35, 10, utf8_decode("Jefe solicitante:"), 0, 0, 'L');
$pdf->Cell(35, 10, $pdf->Image('img/logo.png', $pdf->GetX(), $pdf->GetY(),30), 0, 0, 'L');
$pdf->SetFont("Arial", "", 13);
$pdf->MultiCell(150, 6, utf8_decode($cargo->getDescripcion()), 0, 'L');
$pdf->Ln(); */

/* $pdf -> SetFont("Arial", "B", 13);
$pdf->Cell(25,10,utf8_decode(""),0,0,'L');
$pdf -> SetFont("Arial", "", 13);
$pdf->Cell(20,10,utf8_decode(""),0,0,'L');
$pdf->Ln(); */

//Aspirantes

$pdf->AddPage();
$pdf->SetFont("Times", "I", 25);
$pdf->Cell(216, 15,  utf8_decode("Aspirantes al cargo "), 0, 2, "C");
$pdf->SetDrawColor(200, 100, 0);
$pdf->SetLineWidth(2);
$pdf->Line(10, 30, 195, 30);
$pdf->Ln(10);
$pdf->SetDrawColor(230, 230, 230);
$pdf->SetTextColor(40, 40, 40);
$pdf->SetLineWidth(1);
$pdf->SetFont('Arial', '', 13);

$pdf->Cell(45, 10, "Foto", 1, 0, 'C');
$pdf->Cell(40, 10, "Nombre ", 1, 0, 'C');
$pdf->Cell(50, 10, "Correo ", 1, 0, 'C');
$pdf->Cell(25, 10, "Edad ", 1, 0, 'C');
$pdf->Cell(25, 10, "Civil ", 1, 0, 'C');
$pdf->ln();
foreach ($casps as $actual) {
    $asp = new Aspirante($actual->getIdAspirante());
    $asp->consultar();
    if ($asp->getFoto() == "") {
        $pdf->Cell(45, 35, $pdf->Image("http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user-setting-icon.png", $pdf->GetX() + 12, $pdf->GetY() + 4, 20), 1, 0, 'L');
    } else {
        $pdf->Cell(45, 35, $pdf->Image('img/logo.png', $pdf->GetX() + 12, $pdf->GetY() + 4, 20), 1, 0, 'L');
    }

    $pdf->Cell(40, 35, $asp->getNombre() . " " . $asp->getApellido(), 1, 0, 'C');
    $pdf->Cell(50, 35, $asp->getCorreo(), 1, 0, 'C');
    $pdf->Cell(25, 35, $asp->getEdad(), 1, 0, 'C');
    $pdf->Cell(25, 35, $asp->getCivil(), 1, 0, 'C');
    $pdf->Ln();
}

//Actividades

$pdf->AddPage();
$pdf->SetFont("Times", "I", 25);
$pdf->Cell(216, 15,  utf8_decode("Actividades "), 0, 2, "C");
$pdf->SetDrawColor(100, 200, 100);
$pdf->SetLineWidth(2);
$pdf->Line(10, 30, 195, 30);
$pdf->Ln(10);

$pdf->SetTextColor(40, 40, 40);
$pdf->SetLineWidth(1);
$pdf->SetFont('Arial', '', 13);

$pdf->Cell(45, 10, "Fecha y hora", 'B', 0, 'L');
$pdf->Cell(140, 10, "Actividad ", 'BL', 0, 'L');
$pdf->ln();
foreach ($actividades as $actividadActual) {
    $pdf->Cell(45, 10, $actividadActual->getFecha(), 'R', 0, 'L');
    $actor="";
    switch($actividadActual->getTipoActor()){
        case 'Jefe':{
            $actor = new Jefe($actividadActual->getIdActor());
            $actor->consultar();
        }break;
        case 'Aspirante':{
            $actor = new Aspirante($actividadActual->getIdActor());
            $actor->consultar();
        }break;
        case 'Entrevistador':{
            $actor = new Entrevistador($actividadActual->getIdActor());
            $actor->consultar();
        }break;
        case 'recursos':{
            $actor = new Recursos($actividadActual->getIdActor());
            $actor->consultar();
        }break;
    }
    $pdf->MultiCell(150, 7, utf8_decode("El senor/a ".$actor->getNombre()." ". $actor->getApellido()." ".$actividadActual->getDescripcion()), 'L', 'L');
    $pdf->Ln();
}
$pdf->Output();
ob_end_flush();
