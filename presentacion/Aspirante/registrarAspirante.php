<?php
$error = 0;
$registrado = false;
if (isset($_POST["registrar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $estado = 1;
    $aspirante = new Aspirante("", $nombre, $apellido, $correo, $clave, $estado);
    if ($aspirante->existeCorreo()) {
        $error = 1;
        $registrado = true;
    } else {
        $aspirante->registrar();
        $registrado = true;
    }
    $aspirante = new Aspirante($_SESSION['id']);
    $aspirante->consultar();
}
   include "presentacion/menuInicio.php"; 
?>

<link href="Style/design.css" rel="stylesheet">

<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">

<head>
    <link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
    <script src="librerias/alertifyjs/alertify.js"></script>
    <link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">

    <!--JQUERY-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Nuestro css-->
    <link rel="stylesheet" type="text/css" href="css/index2.css" th:href="@{/css/index2.css}">

    <!-- Estilos registrarse -->
    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="css/estilos.css">

</head>

<body style="background-image: url(img/fondoReg.jpeg);
 background-size:cover;">

    <head>


    </head>

    <body>
        <form class="formulario mt-4" action="index.php?pid=<?php echo base64_encode("presentacion/aspirante/registrarAspirante.php") ?>" method="post">

            <h1>Registrate</h1>
            <div class="contenedor">

                <div class="input-contenedor">
                    <i class="fas fa-user icon"></i>
                    <input class="int" name="nombre" type="text" placeholder="Nombre " required>

                </div>

                <div class="input-contenedor">
                    <i class="fas fa-envelope icon"></i>
                    <input class="int" name="apellido" type="text" placeholder="Apellido" required>

                </div>

                <div class="input-contenedor">
                    <i class="fas fa-envelope icon"></i>
                    <input class="int" name="correo" type="text" placeholder="Correo Electronico" required>

                </div>

                <div class="input-contenedor">
                    <i class="fas fa-key icon"></i>
                    <input class="int" name="clave" type="password" placeholder="Contraseña" required>

                </div>
                <input type="submit" name="registrar" value="Registrate" class="button">
                <p>Al registrarte, aceptas que Punto&Coma acceda a tus datos.</p>
                <p>¿Ya tienes una cuenta?<a class="link" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php") ?>">Iniciar Sesion</a></p>
            </div>
        </form>
        <?php if ($error == 1) { ?>
            <script>
                alertify.error("Este correo ya está en uso");
            </script>
        <?php } else if ($registrado) { ?>
            <script>
                alertify.success("Registro exitoso");
            </script>

        <?php } ?>
    </body>

</html>