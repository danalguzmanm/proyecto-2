<?php
$edad = "";
if (isset($_POST["edad"])) {
	$edad = $_POST["edad"];
}
$sexo = "";
if (isset($_POST["sexo"])) {
	$sexo = $_POST["sexo"];
}
$civil = "";
if (isset($_POST["civil"])) {
	$civil = $_POST["civil"];
}
$descripcion = "";
if (isset($_POST["descripcion"])) {
	$descripcion = $_POST["descripcion"];
}
if (isset($_POST["btnFact"])) { 
	$asp = new Aspirante($_SESSION["id"],"","","","","",$edad, $sexo, $civil, $descripcion);
    $asp->editarA();
    /* $asp = new Aspirante($_GET["idProducto"], $_POST["nombre"], $_POST["cantidad"], $_POST["precio"]);
    $asp->editarA(); */
	} 
?>

<head>
<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
	<script src="librerias/alertifyjs/alertify.js"></script>
	<link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
   <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
</head>

<body style="background-image: url(img/fondo_basic.jpg);
background-size: cover">

</body>

<div class="card-body col-lg-6 col-md-8 col-sm-8 mx-auto rounded-lg mt-5">
<form action="index.php?pid=<?php echo base64_encode("Presentacion/Aspirante/factbasic.php") ?>" method="post">				
<div class="container ">
						<div class="card mt-5 f">
							<div class="card-header bg-dark text-white">
								<h1 class="text-center text-white"><i class="fa fa-id-badge"></i> Datos Basicos</h1>
							</div>
							<div class="card-body">
							<br>
							
								<div class="form-group">
									<input type="number" name="edad" class="form-control" placeholder="Edad" value="<?php echo $edad ?>" required>

								</div>
								<div class="form-group">
									<select class="form-control " name="sexo" placeholder="Sexo" value="<?php echo $sexo ?>" required>>
										<option>Mujer</option>
										<option>Hombre</option>
									</select>
								</div>
								<div class="form-group">
									<div class="form-group">
										<input type="text" name="civil" class="form-control" placeholder="Estado civil" value="<?php echo $civil ?>" required>

                                    </div>
                                    <div class="form-group">
										<input type="text" name="descripcion" class="form-control" placeholder="Descripcion" value="<?php echo $descripcion ?>" required>

									</div>
									<div class="dropdown-divider"></div>
									<button type="submit" name="btnFact" class="btn btn-dark btn-block" class="form-group"><i class="fas fa-check-circle"></i> Enviar</button>
								</div>
							</div>
						</div>
				</form>
			</div>
		</div>
	</div>
	<br><br>
	<div id="alerta" name="alerta">
	<?php if (isset($_POST["btnFact"])) { ?>
		 <script type="text/javascript">
		swal("nice", " Datos actualizados ", "success");
		//Puedes colocar warning, error, success y por último info.
	</script>  
	<?php } ?>

	</div>
	<script>
		function editar(id, obj, val) {
			$(document).ready(function() {
				var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/editar.php") ?>&edic=" + val + "&edit=" + $(obj).text() + "&rol=A" + "&id=" + id;
				$("#alerta").load(url);
			})
		}
	</script>