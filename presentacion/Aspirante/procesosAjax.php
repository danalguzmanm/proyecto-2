<?php
$cantidad = 5;
$pagina = 1;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$id = "";

if (isset($_GET["id"])) {
    $id = $_GET["id"];
} else {
    $id = $_SESSION["id"];
}
$cargo = new Cargo();
$filtro="";
if (isset($_GET["filtro"])) {
    $filtro = $_GET["filtro"];
} 
$totalRegistros = $cargo->consultarCantidadFiltro($filtro);
$cargos = $cargo->consultarPaginacionFiltro($cantidad, $pagina, $filtro);
$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0 || $totalRegistros % $cantidad == 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);

$encuesta = new Encuesta("", $id, "", "", "");
$enc = $encuesta->consultarE();

$cargoasp = new CargoAspirante();
?>

<input id="id" type="hidden" value="<?php echo  $id ?>">
<div id="alerta"></div>
<div id="procesos" style="min-height: 300px;">
    <div class="text-right">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($cargos) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
    <br>
    <div class="table-responsive bg-white border border-secondary" style="border-radius: 8px;">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Cargo</th>
                    <th scope="col">Vacantes</th>
                    <th scope="col">Fecha limite</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Información</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($cargos as $cargoAct) { ?>
                    <tr>
                        <td><?php echo $cargoAct->getNombre() ?></td>
                        <td><?php echo $cargoAct->getVacantes() ?></td>
                        <td><?php echo $cargoAct->getFecha() ?></td>
                        <td><?php echo $cargoAct->getEstadp() ?></td>
                        <td><?php echo "<button id='" . $cargoAct->geIdCargo() . "' class='btn btn-dark rounded-pill' onclick='modal(this.id)'><i class='fas fa-info-circle'></i></button>" ?></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-9">
        <nav>
            <ul class="pagination">
                <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><button class="page-link" onclick="anterior()"> &lt;&lt; </button></li>
                <?php
                for ($i = 1; $i <= $totalPaginas; $i++) {
                    if ($i == $pagina) {
                        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                    } else {
                        echo "<li class='page-item'><button class='page-link' onclick='elegirpage(this)'>" . $i . "</button></li>";
                    }
                }
                ?>
                <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><button class="page-link" onclick="siguiente()"> &gt;&gt; </button></li>
            </ul>
        </nav>
    </div>
    <div class="col-3 d-flex justify-content-end">
        <select id="cantidad" class="form-control" style="width: 60px;" onchange="cantidad(this.value)">
            <option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
            <option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
            <option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
            <option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
        </select>
    </div>
</div>
<!-- Modal información -->
<div class="modal fade mt-5" id="info" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="exampleModalLabel">Informacion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input id="cargo" type="hidden" value="">
            <div class="modal-body">
                <div id="modalinfo">

                </div>
            </div>
            <div class="modal-footer">
                <?php
                if ($enc < 3) {
                    echo "<div class='alert alert-danger' role='alert'>
                    Completa las <b><a href=index.php?pid=" . base64_encode("Presentacion/encuesta/inicio.php") . ">encuestas</a></b> de tu perfil para poder aspirar a una vacante
                  </div>";
                } else {
                ?>
                    <button type="button" class="btn btn-dark" onclick="Postularse()">Aspirar</button>
                <?php
                }
                ?>

            </div>
        </div>
    </div>
</div>

<script>
    function siguiente() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/aspirante/procesosAjax.php") . "&pagina=" . ($pagina + 1); ?>" + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function elegirpage(obj) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/aspirante/procesosAjax.php") ?>&pagina=" + $(obj).text() + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function anterior() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/aspirante/procesosAjax.php") . "&pagina=" . ($pagina); ?>" + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function cantidad(val) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/aspirante/procesosAjax.php"); ?>" + "&cantidad=" + val + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function modal(id) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/jefe/cargo.php"); ?>&id=" + id;
        $("#modalinfo").load(url);
        $('#cargo').val(id);
        $('#info').modal('show');
    }

    function Postularse() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/aspirante/postularse.php") . "&id=" . $_SESSION["id"]; ?>&idC=" + $("#cargo").val();
        $("#alerta").load(url);
        $('#info').modal('hide');
    }
</script>