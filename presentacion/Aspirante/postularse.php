<?php
$fecha = new DateTime();
$ident = $fecha->getTimestamp();
if (isset($_GET["id"])) {
    $aspcargo = new CargoAspirante($_GET["idC"], $_GET["id"]);
    $aspcargo->insertar();
    $contra = new Contratacion("", "", $_GET["idC"]);
    $contra->consultar();
    $actividad = new Actividad("", "", "se postuló al cargo", $contra->getIdContratacion(), $_GET["id"], "Aspirante");
    $actividad->insertar();
    $log = new Log($ident, "", "", "Cargo solicitado: ".$_GET["idC"], "4");
    $log->insertar();
    $log->asociarA($_GET["id"]);
};
?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
		swal("¡Hecho!", " Te has postulado al cargo ", "success");
		//Puedes colocar warning, error, success y por último info.
	</script> 