<?php
$img_nomb = "predeterminada.png";
$src = "";
$tiempo = new DateTime();
$destino = "img/siempre/";
if (isset($_POST["crear"])) {
	if ($_FILES["foto"]["name"] != "") {
		$auxfoto = $_FILES["foto"]["tmp_name"];
		$type_foto = $_FILES["foto"]["type"];
		$img_nomb = "img_" . $tiempo->getTimestamp() . (($type_foto == "img/png") ? ".png" : ".jpg");
		$src = $destino . $img_nomb;
		copy($auxfoto, $src);
	} else {
		$src = $destino . $img_nomb;
	}
	$aspirante1 = new Aspirante($_SESSION["id"]);
	$aspirante1->consultar();
	if (($aspirante1->getFoto() != "") && (strpos($aspirante1->getFoto(), "predeterminada.png") != true)) {
		unlink($aspirante1->getFoto());
	}
	$asp = new Aspirante($_SESSION["id"], "", "", "", "", $src);
	$asp->editarFoto();
}
$aspirante = new Aspirante($_SESSION["id"]);
$aspirante->consultar();

?>
<html>

<head>
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
	<script src="librerias/alertifyjs/alertify.js"></script>
	<link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
</head>

<body style="background-image: url(img/fondoAsp.jpg);
background-size: cover">



	<div class="container ">
		<div class="row">
			<div class="card-body col-lg-6 col-md-8 col-sm-8 mx-auto rounded-lg mt-5">
				<div class="card-header text-white text-center bg-dark">
				<h1 class="text-center text-white"><i class="fa fa-id-badge"></i> Datos Aspirante</h1>
				</div>
				<div class="card-body f">
					<div class="row ">
						<div class="col-lg-8 mx-auto col-md-8 col-sm-8">
							<img src="<?php echo ($aspirante->getFoto() != "") ? $aspirante->getFoto() :
											"http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user-setting-icon.png"; ?>" width="100%" class="img-thumbnail">

							<form action="index.php?pid=<?php echo base64_encode("Presentacion/Aspirante/perfilAspirante.php") ?>" method="post" enctype="multipart/form-data">
								<div class="input-group mb-">
									<div class="input-group-prepend">
										<span class="input-group-text" id="inputGroupFileAddon01">Subir</span>
									</div>
									<div class="custom-file">
										<input type="file" class="custom-file-input" name="foto" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
										<label class="custom-file-label" for="inputGroupFile01">Seleccionar imagen</label>
									</div>
									<button type="submit" name="crear" class="btn btn-dark btn-block" class="form-group"><i class="fa fa-camera"></i> Agregar</button>
								</div>
							</form>
						</div>
					</div>

					<div class="dropdown-divider"></div>
					<div class="row">
						<div class="col">
							<div class="card mx-auto col-lg-8 col-md-8 col-sm-12 bg-white">
								<table class="table table-hover">
									<tr>
										<th>Nombre</th>
										<td id="<?php echo $aspirante->getIdAspirante() ?>" contenteditable="true" onblur="editar(this.id,this,'nombre')"><?php echo $aspirante->getNombre() ?></td>
									</tr>
									<tr>
										<th>Apellido</th>
										<td id="<?php echo $aspirante->getIdAspirante() ?>" contenteditable="true" onblur="editar(this.id,this,'apellido')"><?php echo $aspirante->getApellido() ?></td>
									</tr>
									<tr>
										<th>Correo</th>
										<td><?php echo $aspirante->getCorreo() ?></td>
									</tr>
								</table>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>
		<div id="alerta" name="alerta"></div>
	</div>
	<script>
		function editar(id, obj, val) {
			$(document).ready(function() {
				var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/editar.php") ?>&edic=" + val + "&edit=" + $(obj).text() + "&rol=A" + "&id=" + id;
				$("#alerta").load(url);
			})
		}
	</script>