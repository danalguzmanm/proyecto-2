<body style="background-image: url(img/fondo_basic.jpg);
background-size: cover">
<head>
<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
	<script src="librerias/alertifyjs/alertify.js"></script>
	<link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
</head>
    <div class="container mt-5">
        <div class="card">
            <div class="card-header bg-success text-white">
                <h4>Test de personalidad</h4>
            </div>
            <div class="card-body">
                <div>
                    <p>
                        <b>1. </b> Si pudieras gastar mucho dinero en uno de los siguientes artículos, ¿qué elegirías?
                    </p>
                    <form id="pregunta1" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="1">
                            <label class="form-check-label" for="pregunta1">
                                Un colchón nuevo
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="2">
                            <label class="form-check-label" for="pregunta1">
                                Un estéreo nuevo
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="3">
                            <label class="form-check-label" for="pregunta1">
                                Un televisor nuevo
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>2. </b>¿Cuál es tu elección para pasar una tarde tranquila?
                    </p>
                    <form id="pregunta2" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="1">
                            <label class="form-check-label" for="pregunta2">
                                Quedarte en casa y comer comida casera
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="2">
                            <label class="form-check-label" for="pregunta2">
                                Ir a un concierto
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="3">
                            <label class="form-check-label" for="pregunta2">
                                Ir al cine
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>3. </b>¿Cómo prefieres pasar un rato libre?
                    </p>
                    <form id="pregunta3" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="1">
                            <label class="form-check-label" for="pregunta3">
                                Ir a la presentación de un libro
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="2">
                            <label class="form-check-label" for="pregunta3">
                                Hacer un paseo por los alrededores
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="3">
                            <label class="form-check-label" for="pregunta3">
                                Descansar y no ir a ninguna parte
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>4. </b>De vacaciones, ¿qué tipo de habitación solicitas en el hotel en el que te hospedas?
                    </p>
                    <form id="pregunta4" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="1">
                            <label class="form-check-label" for="pregunta4">
                                Con vista panorámica de la ciudad
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="2">
                            <label class="form-check-label" for="pregunta4">
                                Que esté lo más cerca de la playa
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="3">
                            <label class="form-check-label" for="pregunta4">
                                Totalmente alejada del bullicio y del exterior
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>5. </b>¿Qué tipo de reunión social prefieres?
                    </p>
                    <form id="pregunta5" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="1">
                            <label class="form-check-label" for="pregunta5">
                                Una boda
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="2">
                            <label class="form-check-label" for="pregunta5">
                                Una exposición de pintura
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="3">
                            <label class="form-check-label" for="pregunta5">
                                Una pequeña fiesta
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>6. </b>De estas tres opciones, ¿cuál te define mejor?
                    </p>
                    <form id="pregunta6" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="1">
                            <label class="form-check-label" for="pregunta6">
                                Atlético
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="2">
                            <label class="form-check-label" for="pregunta6">
                                Intelectual
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="3">
                            <label class="form-check-label" for="pregunta6">
                                Humanitario
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>7. </b>¿Cuál es tu método de comunicación preferido?

                    </p>
                    <form id="pregunta7" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="1">
                            <label class="form-check-label" for="pregunta7">
                                Correo electrónico o carta
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="2">
                            <label class="form-check-label" for="pregunta7">
                                Teléfono
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="3">
                            <label class="form-check-label" for="pregunta7">
                                Persona a persona
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>8. </b>Si no encuentras tus llaves, ¿qué haces?
                    </p>
                    <form id="pregunta8" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="1">
                            <label class="form-check-label" for="pregunta8">
                                Las buscas mirando por todos lados
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="2">
                            <label class="form-check-label" for="pregunta8">
                                Sacudes los bolsillos o la cartera para oír el ruido
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="3">
                            <label class="form-check-label" for="pregunta8">
                                Buscas al tacto
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>9. </b>Si obtuvieras el premio mayor, ¿qué harías con el dinero?
                    </p>
                    <form id="pregunta9" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="1">
                            <label class="form-check-label" for="pregunta9">
                                Comprar una hermosa casa y quedarte ahí
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="2">
                            <label class="form-check-label" for="pregunta9">
                                Viajar
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="3">
                            <label class="form-check-label" for="pregunta9">
                                Introducirte al mundo social
                            </label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success rounded-pill" type="submit" onclick="enviar()">Enviar</button>
            </div>
        </div>
    </div>
    <script>
        function enviar() {
            var total = 0;
            var x = false;
            for (i = 1; i <= 9; i++) {
                if ($("#pregunta" + i + ":checked").val() == null) {
                    x = true;
                }
            }
            if (x == true) {
                alert("Complete todas las respuestas");
                return;
            }
            for (i = 1; i <= 9; i++) {
                var texto = parseInt($("#pregunta" + i + ":checked").val(), 10);
                total = total + texto;
            }
            url = "index.php?pid=<?php echo base64_encode("presentacion/encuesta/inicio.php") ?>&t=1&p="+total;
            window.location.replace(url);
        }
    </script>