<?php
$enc = new Encuesta();
$enc1 = $enc->consultarPerson($idCargo);
$enc2 = new Encuesta();
$enc21 = $enc2->consultarOtra($idCargo);
$enc3 = new Encuesta();
$enc31 = $enc3->consultarEspa($idCargo);
$enc4 = new Encuesta();
$enc41 = $enc4->consultarEn($idCargo);
$visual = 0;
$auditivo = 0;
$Kinestesico = 0;
$VisualN = "";
$auditivoN = "";
$KinestesicoN = "";
foreach ($enc1 as $prodActucal) {
    $obj = new Aspirante($prodActucal->getIdAspirante());
    $obj->consultar();
    if ($prodActucal->getPuntaje() <= 9) {
        $visual++;
        $VisualN = $VisualN . $obj->getNombre() . $obj->getApellido() . ",";
    } else if ($prodActucal->getPuntaje() <= 18) {
        $auditivo++;
        $auditivoN = $auditivoN . $obj->getNombre() . $obj->getApellido() . ",";
    } else if ($prodActucal->getPuntaje() <= 27) {
        $Kinestesico++;
        $KinestesicoN = $KinestesicoN . $obj->getNombre() . $obj->getApellido() . ",";
    }
}
?>
<html>

<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Personalidad', 'Aspirante'],

                <?php

                echo "['visual'," . $visual . "],";
                echo "['auditivo',"  . $auditivo . "],";
                echo "['kinestesico'," . $Kinestesico . "],";
                ?>

            ]);

            var options = {
                title: 'Grafica de encuestas',
                is3D: true,
                width: 700,
                height: 300
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }
        google.charts.load('current', {
            packages: ['corechart', 'bar']
        });
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {
            var data = google.visualization.arrayToDataTable([
                ['Aspirante', 'Puntuacion', ],
                <?php
                foreach ($enc21 as $prodActucal) {
                    $asp = new Aspirante($prodActucal->getIdAspirante());
                    $asp->consultar();
                    echo "['" . $asp->getNombre() . "', " . $prodActucal->getPuntaje() . "],";
                }
                ?>
            ]);

            var options = {
                title: 'Razonamiento',
                width: 1030,
                height: 300,
                chartAre: {
                    width: '80%',
                    height: '100%'
                },
                hAxis: {
                    title: 'Aspirante',
                    minValue: 0
                },
                vAxis: {
                    title: 'Puntaje'
                }
            };

            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
        google.charts.load('current', {
            packages: ['corechart', 'bar']
        });
        google.charts.setOnLoadCallback(drawColColors);

        function drawColColors() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Aspirante');
            data.addColumn('number', 'Puntaje ');
            data.addRows([
                <?php
                foreach ($enc31 as $prodActucal) {
                    $asp = new Aspirante($prodActucal->getIdAspirante());
                    $asp->consultar();
                    echo "['" . $asp->getNombre() . "', " . $prodActucal->getPuntaje() . "],";
                }
                ?>
            ]);

            var options = {
                title: 'Grafica Espacial',
                colors: ['#9575cd', '#33ac71'],
                hAxis: {
                    title: 'Aspirantes',
                    format: 'h:mm a',
                    viewWindow: {
                        min: [7, 30, 0],
                        max: [17, 30, 0]
                    },
                    width: '80%',
                    height: '100%'
                },
                vAxis: {
                    title: 'Puntaje'
                },
                width: 1030,
                height: 300,
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
            chart.draw(data, options);
        }

        function drawChart2() {
            var data = google.visualization.arrayToDataTable([
                ['Personalidad', 'Aspirante'],
                <?php
                foreach ($enc41 as $prodActucal) {
                    $asp = new Aspirante($prodActucal->getIdAspirante());
                    $asp->consultar();
                    echo "['" . $asp->getNombre() . "', " . $prodActucal->getPuntaje() . "],";
                }
                ?>
            ]);

            var options = {
                title: 'Entrevistas',
                viewWindow: {
                    min: [7, 30, 0],
                    max: [17, 30, 0]
                },
                width: 700,
                height: 300,
                is3D: true,
                pieHole: 0.5,
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
            chart.draw(data, options);
        }
        google.charts.setOnLoadCallback(drawChart2);
    </script>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <div class=" b border border-secondary overflow-auto">
                    <div id="piechart"></div>
                </div>

            </div>
            <div class="col">
                <div class="table-responsive bg-white border border-secondary" style="border-radius: 8px;">
                    <table class="table ">
                        <thead>
                            <tr>
                                <th scope="col">Tipo de Personalidad</th>
                                <th scope="col">Aspirante</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo "visual" ?></td>
                                <td><?php echo $VisualN ?></td>
                            </tr>
                            <tr>
                                <td><?php echo "auditivo" ?></td>
                                <td><?php echo $auditivoN ?></td>
                            </tr>
                            <tr>
                                <td><?php echo "Kinestesico" ?></td>
                                <td><?php echo $KinestesicoN ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class=" b border border-secondary mt-4 overflow-auto">
            <div id="chart_div"></div>
        </div>
        <div class=" b border border-secondary mt-4 overflow-auto">
            <div id="chart_div2"></div>
        </div>
        <div class=" b border border-secondary mt-4 overflow-auto bg-white">
            <div id="donutchart"></div>
        </div>


    </div>

</body>

</html>