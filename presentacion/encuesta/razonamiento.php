<body style="background-image: url(img/fondo_basic.jpg);
background-size: cover">
    <div class="container mt-5">
        <div class="card">
            <div class="card-header bg-warning">
                <h4>Test de razonamiento</h4>
            </div>
            <div class="card-body">
                <p>Resulve las igualdades para encontrar las incognitas</p>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>1. </b> <img src="img/razonamiento/p1.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta1" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="1">
                                <label class="form-check-label" for="pregunta1">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="0">
                                <label class="form-check-label" for="pregunta1">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="0">
                                <label class="form-check-label" for="pregunta1">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="0">
                                <label class="form-check-label" for="pregunta1">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="0">
                                <label class="form-check-label" for="pregunta1">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>2. </b> <img src="img/razonamiento/p2.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta2" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="0">
                                <label class="form-check-label" for="pregunta2">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="0">
                                <label class="form-check-label" for="pregunta2">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="0">
                                <label class="form-check-label" for="pregunta2">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="0">
                                <label class="form-check-label" for="pregunta2">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="1">
                                <label class="form-check-label" for="pregunta2">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>3. </b> <img src="img/razonamiento/p3.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta3" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="0">
                                <label class="form-check-label" for="pregunta3">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="1">
                                <label class="form-check-label" for="pregunta3">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="0">
                                <label class="form-check-label" for="pregunta3">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="0">
                                <label class="form-check-label" for="pregunta3">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="0">
                                <label class="form-check-label" for="pregunta3">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>4. </b> <img src="img/razonamiento/p4.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta4" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="0">
                                <label class="form-check-label" for="pregunta4">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="1">
                                <label class="form-check-label" for="pregunta4">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="0">
                                <label class="form-check-label" for="pregunta4">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="0">
                                <label class="form-check-label" for="pregunta4">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="0">
                                <label class="form-check-label" for="pregunta4">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>5. </b> <img src="img/razonamiento/p5.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta5" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="0">
                                <label class="form-check-label" for="pregunta5">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="0">
                                <label class="form-check-label" for="pregunta5">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="1">
                                <label class="form-check-label" for="pregunta5">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="0">
                                <label class="form-check-label" for="pregunta5">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="0">
                                <label class="form-check-label" for="pregunta5">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>6. </b> <img src="img/razonamiento/p6.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta6" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="0">
                                <label class="form-check-label" for="pregunta6">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="0">
                                <label class="form-check-label" for="pregunta6">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="1">
                                <label class="form-check-label" for="pregunta6">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="0">
                                <label class="form-check-label" for="pregunta6">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="0">
                                <label class="form-check-label" for="pregunta6">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>7. </b> <img src="img/razonamiento/p7.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta7" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="0">
                                <label class="form-check-label" for="pregunta7">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="0">
                                <label class="form-check-label" for="pregunta7">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="1">
                                <label class="form-check-label" for="pregunta7">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="0">
                                <label class="form-check-label" for="pregunta7">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="0">
                                <label class="form-check-label" for="pregunta7">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>8. </b> <img src="img/razonamiento/p8.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta8" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="0">
                                <label class="form-check-label" for="pregunta8">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="1">
                                <label class="form-check-label" for="pregunta8">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="0">
                                <label class="form-check-label" for="pregunta8">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="0">
                                <label class="form-check-label" for="pregunta8">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="0">
                                <label class="form-check-label" for="pregunta8">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>9. </b> <img src="img/razonamiento/p9.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta9" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="1">
                                <label class="form-check-label" for="pregunta9">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="0">
                                <label class="form-check-label" for="pregunta9">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="0">
                                <label class="form-check-label" for="pregunta9">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="0">
                                <label class="form-check-label" for="pregunta9">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="0">
                                <label class="form-check-label" for="pregunta9">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>
                            <b>10. </b> <img src="img/razonamiento/p10.png" class="" alt="...">
                        </p>
                    </div>
                    <div class="col-6 ">
                        <form id="pregunta10" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta10" value="0">
                                <label class="form-check-label" for="pregunta10">
                                    5
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta10" value="0">
                                <label class="form-check-label" for="pregunta10">
                                    4
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta10" value="1">
                                <label class="form-check-label" for="pregunta10">
                                    3
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta10" value="0">
                                <label class="form-check-label" for="pregunta10">
                                    2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta10" value="0">
                                <label class="form-check-label" for="pregunta10">
                                    1
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <!--  -->
            </div>
            <div class="card-footer">
                <button class="btn btn-warning rounded-pill" type="submit" onclick="enviar()">Enviar</button>
            </div>
        </div>
    </div>
    <script>
        function enviar() {
            var total = 0;
            var x = false;
            for (i = 1; i <= 10; i++) {
                if ($("#pregunta" + i + ":checked").val() == null) {
                    x = true;
                }
            }
            if (x == true) {
                alert("Complete todas las respuestas");
                return;
            }
            for (i = 1; i <= 10; i++) {
                var texto = parseInt($("#pregunta" + i + ":checked").val(), 10);
                total = total + texto;
            }
            url = "index.php?pid=<?php echo base64_encode("presentacion/encuesta/inicio.php") ?>&t=2&p="+total;
            window.location.replace(url);
        }
    </script>