<input type="hidden" id=idas value="<?php echo $_GET["idas"] ?>">
<body style="background-image: url(img/fondo_basic.jpg);
background-size: cover">
<head>
<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
	<script src="librerias/alertifyjs/alertify.js"></script>
	<link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
</head>

    <div class="container mt-5">
        <div class="card">
            <div class="card-header bg-success text-white">
                <h4>Entrevista Para Aspirantes</h4>
            </div>
            <div class="card-body">
                <div>
                    <p>
                        <b>1. </b> ¿Por qué te interesa trabajar en esta empresa? 
                    </p>
                    <form id="pregunta1" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="3">
                            <label class="form-check-label" for="pregunta1">
                                convincente
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="2">
                            <label class="form-check-label" for="pregunta1">
                                Poco convincente
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="1">
                            <label class="form-check-label" for="pregunta1">
                                No convencio
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>2. </b>¿Qué sabes de nuestra empresa?
                    </p>
                    <form id="pregunta2" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="3">
                            <label class="form-check-label" for="pregunta2">
                                Bien informado
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="2">
                            <label class="form-check-label" for="pregunta2">
                                Poco informado
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="1">
                            <label class="form-check-label" for="pregunta2">
                                Nada informado
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>3. </b>¿Cuáles son tus pretensiones salariales?
                    </p>
                    <form id="pregunta3" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="3">
                            <label class="form-check-label" for="pregunta3">
                                Muy altas
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="2">
                            <label class="form-check-label" for="pregunta3">
                                Poco altas
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="1">
                            <label class="form-check-label" for="pregunta3">
                                Bajas
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>4. </b>Menciona las situaciones concretas de tu vida laboral en la que hayas demostrado tu liderazgo
                    </p>
                    <form id="pregunta4" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="3">
                            <label class="form-check-label" for="pregunta4">
                                Buen liderazgo
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="2">
                            <label class="form-check-label" for="pregunta4">
                                Poco liderazgo
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="1">
                            <label class="form-check-label" for="pregunta4">
                                Nada de liderazgo
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>5. </b>¿Cuanto tiempo duraste en tu anterior empleo?
                    </p>
                    <form id="pregunta5" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="3">
                            <label class="form-check-label" for="pregunta5">
                                Mucho tiempo
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="2">
                            <label class="form-check-label" for="pregunta5">
                                Poco tiempo
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="1">
                            <label class="form-check-label" for="pregunta5">
                                Primer empleo
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>6. </b>¿Por qué deberíamos contratarte a ti y no a otros candidatos?
                    </p>
                    <form id="pregunta6" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="3">
                            <label class="form-check-label" for="pregunta6">
                                convincente
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="2">
                            <label class="form-check-label" for="pregunta6">
                                Poco convincente
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="1">
                            <label class="form-check-label" for="pregunta6">
                                No convencio
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>7. </b>¿En que consistirá tu trabajo?

                    </p>
                    <form id="pregunta7" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="3">
                            <label class="form-check-label" for="pregunta7">
                                Sabe en que consiste
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="2">
                            <label class="form-check-label" for="pregunta7">
                                Poco sabe en que consiste
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="1">
                            <label class="form-check-label" for="pregunta7">
                                No sabe en que consiste
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>8. </b>¿Qué idiomas habla?
                    </p>
                    <form id="pregunta8" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="3">
                            <label class="form-check-label" for="pregunta8">
                                Dos o mas
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="2">
                            <label class="form-check-label" for="pregunta8">
                                Español y otro
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="1">
                            <label class="form-check-label" for="pregunta8">
                                Español solamente
                            </label>
                        </div>
                    </form>
                </div>
                <div>
                    <p>
                        <b>9. </b>¿Que experiencia tiene en este sector?
                    </p>
                    <form id="pregunta9" class="ml-5">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="3">
                            <label class="form-check-label" for="pregunta9">
                                Bastante
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="2">
                            <label class="form-check-label" for="pregunta9">
                                Pocas
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta9" value="1">
                            <label class="form-check-label" for="pregunta9">
                                Ninguna
                            </label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success rounded-pill"  type="submit" onclick="enviar()">Enviar</button>
            </div>
        </div>
    </div>
    <?php if (isset($_GET[""])) { ?>
		<script>
		  alertify.success("Datos registrados");
		</script>
	<?php } ?>
    <script>
        function enviar() {
            var total = 0;
            var x = false;
            for (i = 1; i <= 9; i++) {
                if ($("#pregunta" + i + ":checked").val() == null) {
                    x = true;
                }
            }
            if (x == true) {
                alert("Complete todas las respuestas");
                return;
            }
            for (i = 1; i <= 9; i++) {
                var texto = parseInt($("#pregunta" + i + ":checked").val(), 10);
                total = total + texto;
            }
            url = "index.php?pid=<?php echo base64_encode("presentacion/entrevistador/entrevistas.php")?>&t=entrevista&p="+total+"&id2="+$("#idas").val();
            window.location.replace(url);
            alertify.success("Datos registrados");
        }
    </script>