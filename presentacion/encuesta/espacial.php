<body style="background-image: url(img/fondo_basic.jpg);
background-size: cover">
    <div class="container mt-5">
        <div class="card">
            <div class="card-header bg-info text-white">
                <h4>Test aptitud espacial</h4>
            </div>
            <div class="card-body">
                <p>Averigua que figuras podrian formarse a partir de la figura desplegada</p>
                <p>
                    <b>1. </b> <img src="img/espacial/p1.png" class="" alt="...">
                </p>

                <form id="pregunta1" class="ml-5">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="0">
                        <label class="form-check-label" for="pregunta1">
                            a
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="0">
                        <label class="form-check-label" for="pregunta1">
                            b
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="0">
                        <label class="form-check-label" for="pregunta1">
                            c
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta1" value="1">
                        <label class="form-check-label" for="pregunta1">
                            d
                        </label>
                    </div>
                </form>

                <p>
                    <b>2. </b> <img src="img/espacial/p2.png" class="" alt="...">
                </p>

                <form id="pregunta2" class="ml-5">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="0">
                        <label class="form-check-label" for="pregunta2">
                            a
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="0">
                        <label class="form-check-label" for="pregunta2">
                            b
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="0">
                        <label class="form-check-label" for="pregunta2">
                            c
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta2" value="1">
                        <label class="form-check-label" for="pregunta2">
                            d
                        </label>
                    </div>
                </form>
                        <p>
                            <b>3. </b> <img src="img/espacial/p3.png" class="" alt="...">
                        </p>
                        <form id="pregunta3" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="0">
                                <label class="form-check-label" for="pregunta3">
                                    a
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="0">
                                <label class="form-check-label" for="pregunta3">
                                    b
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="1">
                                <label class="form-check-label" for="pregunta3">
                                    c
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta3" value="0">
                                <label class="form-check-label" for="pregunta3">
                                    d
                                </label>
                            </div>
                        </form>
                        <p>
                            <b>4. </b> <img src="img/espacial/p4.png" class="" alt="...">
                        </p>

                        <form id="pregunta4" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="0">
                                <label class="form-check-label" for="pregunta4">
                                    a
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="0">
                                <label class="form-check-label" for="pregunta4">
                                    b
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="0">
                                <label class="form-check-label" for="pregunta4">
                                    c
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta4" value="1">
                                <label class="form-check-label" for="pregunta4">
                                    d
                                </label>
                            </div>
                        </form>

                        <p>
                            <b>5. </b> <img src="img/espacial/p5.png" class="" alt="...">
                        </p>

                        <form id="pregunta5" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="0">
                                <label class="form-check-label" for="pregunta5">
                                    a
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="0">
                                <label class="form-check-label" for="pregunta5">
                                    b
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="0">
                                <label class="form-check-label" for="pregunta5">
                                    c
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta5" value="1">
                                <label class="form-check-label" for="pregunta5">
                                    d
                                </label>
                            </div>
                        </form>

                        <p>
                            <b>6. </b> <img src="img/espacial/p6.png" class="" alt="...">
                        </p>

                        <form id="pregunta6" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="0">
                                <label class="form-check-label" for="pregunta6">
                                    a
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="0">
                                <label class="form-check-label" for="pregunta6">
                                    b
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="0">
                                <label class="form-check-label" for="pregunta6">
                                    c
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta6" value="1">
                                <label class="form-check-label" for="pregunta6">
                                    d
                                </label>
                            </div>
                        </form>

                        <p>
                            <b>7. </b> <img src="img/espacial/p7.png" class="" alt="...">
                        </p>

                        <form id="pregunta7" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="0">
                                <label class="form-check-label" for="pregunta7">
                                    a
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="0">
                                <label class="form-check-label" for="pregunta7">
                                    b
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="0">
                                <label class="form-check-label" for="pregunta7">
                                    c
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta7" value="1">
                                <label class="form-check-label" for="pregunta7">
                                    d
                                </label>
                            </div>
                        </form>

                        <p>
                            <b>8. </b> <img src="img/espacial/p8.png" class="" alt="...">
                        </p>

                        <form id="pregunta8" class="ml-5">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="0">
                                <label class="form-check-label" for="pregunta8">
                                    a
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="0">
                                <label class="form-check-label" for="pregunta8">
                                    b
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="0">
                                <label class="form-check-label" for="pregunta8">
                                    c
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="pregunta8" value="1">
                                <label class="form-check-label" for="pregunta8">
                                    d
                                </label>
                            </div>
                        </form>

                <!--  -->
            </div>
            <div class="card-footer">
                <button class="btn btn-warning rounded-pill" type="submit" onclick="enviar()">Enviar</button>
            </div>
        </div>
    </div>
    <script>
        function enviar() {
            var total = 0;
            var x = false;
            for (i = 1; i <= 8; i++) {
                if ($("#pregunta" + i + ":checked").val() == null) {
                    x = true;
                }
            }
            if (x == true) {
                alert("Complete todas las respuestas");
                return;
            }
            for (i = 1; i <= 8; i++) {
                var texto = parseInt($("#pregunta" + i + ":checked").val(), 10);
                total = total + texto;
            }
            url = "index.php?pid=<?php echo base64_encode("presentacion/encuesta/inicio.php") ?>&t=3&p=" + total;
            window.location.replace(url);
        }
    </script>