<?php
if (isset($_GET["t"])) {
    $tipo = "";
    switch ($_GET["t"]) {
        case ("1"):
            $tipo = "Personalidad";
            break;
        case ("2"):
            $tipo = "Razonamiento";
            break;
        case ("3"):
            $tipo = "Espacial";
            break;
    }
    $fecha = new DateTime();
    $id = $fecha->getTimestamp();
    $ident = $fecha->getTimestamp();
    $rec = new Entrevistador();
    $recs = $rec->consultarTodosId();
    $rand = rand(0, count($recs) - 1);
    $codrec = $recs[$rand]->getIdRecursos();
    $encuesta = new Encuesta($id, $_SESSION["id"], $tipo, $_GET["p"], $codrec);
    $encuesta->eliminar();
    $encuesta->insertar();
    $log = new Log($ident, "", "", "Resolvio encuesta de ".$tipo." y obtuvo un puntaje de ".$_GET["p"], "2");
    $log->insertar();
    $log->asociarA($_SESSION["id"]);
}
?>

<body style="background-image: url(img/fondoEnc.jpg);
background-size: cover">
    <div class="container mt-5">
        <div class="card f">
            <div class="card-header bg-dark text-white">
            <h4 class="text-white"><i class="fa fa-comments"></i> Encuestas</h4>
            </div>
            <div class="card-body f">
                
                <div class="row mt-3">
                    <div class="col-4">
                        <div class="card">
                            <div class="m-4">
                                <img src="img/razonamieto.png" class="card-img-top rounded-circle" alt="...">
                            </div>
                            <div class="card-body ">
                                <div class="d-flex justify-content-center">
                                    <h5 class="card-title">Test de razonamieto</h5>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <a href="index.php?pid=<?php echo base64_encode("Presentacion/encuesta/razonamiento.php") ?>" class="btn btn-warning rounded-pill">Diligenciar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card">
                            <div class="m-4">
                                <img src="img/Personalidad.png" class="card-img-top rounded-circle" alt="...">
                            </div>
                            <div class="card-body ">
                                <div class="d-flex justify-content-center">
                                    <h5 class="card-title">Test de personalidad</h5>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <a href="index.php?pid=<?php echo base64_encode("Presentacion/encuesta/personalidad.php") ?>" class="btn btn-success rounded-pill">Diligenciar</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="card">
                            <div class="m-4">
                                <img src="img/espacial.png" class="card-img-top rounded-circle" alt="...">
                            </div>
                            <div class="card-body ">
                                <div class="d-flex justify-content-center">
                                    <h5 class="card-title">Test aptitud espacial</h5>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <a href="index.php?pid=<?php echo base64_encode("Presentacion/encuesta/espacial.php") ?>" class="btn btn-info rounded-pill">Diligenciar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">

            </div>
        </div>

    </div>