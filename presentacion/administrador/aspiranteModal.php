<?php
$aspirante = new Aspirante($_GET["id"]);
$aspirante->consultar();
?>
<div class="modal-content b">
    <div class="modal-header b">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo $aspirante->getNombre() . " " . $aspirante->getApellido() ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <input id="cargo" type="hidden" value="">
    <div class="modal-body">
        <div class="row">
            <div class="col-4 col-xs-12 ">
                <img src="<?php echo ($aspirante->getFoto() != "") ? $aspirante->getFoto() :
                                "http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user-setting-icon.png"; ?>" width="100%" class="img-thumbnail">
            </div>
            <div class="col-8 col-xs-12">
                
                <div class="card border border-secondary" style="border-radius: 15px;">
                <h4 class="card-title text-center"><?php echo "Informacion básica" ?></h4>
                    <div class="m-2">
                        <p class="card-text"><b>Correo: </b><?php echo $aspirante->getCorreo() ?></p>
                        <p class="card-text"><b>Edad: </b><?php echo $aspirante->getEdad() ?></p>
                        <p class="card-text"><b>Sexo: </b><?php echo $aspirante->getSexo() ?></p>
                        <p class="card-text"><b>Civil: </b><?php echo $aspirante->geTCivil() ?></p>
                        <p class="card-text"><b>Descripcion: </b><?php echo $aspirante->getDescripcion() ?></p>
                    </div>


                </div>

            </div>
        </div>
    </div>
    <div class="modal-footer">

    </div>
</div>