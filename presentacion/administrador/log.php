<?php

?>

<body style="background-image: url(img/fondoPerfil.jpg);
background-size: cover">
    <div class="container mt-5 mb-5">
        <div class="card f3 b">
            <div class="card-header bg-dark text-white text-center" style="border-top-left-radius: 8px; border-top-right-radius: 8px;">
                <h2 class="text-center text-white"><i class="fa fa-users"></i> Log</h2>
                <ul class="nav nav-tabs card-header-tabs" id="bologna-list" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#aspirantes" role="tab" aria-controls="description" aria-selected="true" onclick="recargar(1)">Aspirantes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#jefes" role="tab" aria-controls="history" aria-selected="false">Jefes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#recursos" role="tab" aria-controls="deals" aria-selected="false">R. Humanos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#entrevistador" role="tab" aria-controls="deals" aria-selected="false">Entrevistadores</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#administrador" role="tab" aria-controls="deals" aria-selected="false">Administradores</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content mt-3">
                    <div class="tab-pane active" id="aspirantes" role="tabpanel">
                        <div>
                            <input id="filtroA" type="text" class="form-control" placeholder="Buscar" onkeyup="BuscarAsp(this.value)">
                        </div>
                        <div id="procesosA">

                        </div>
                    </div>

                    <div class="tab-pane" id="jefes" role="tabpanel" aria-labelledby="history-tab">
                        <div>
                            <input id="filtroJ" type="text" class="form-control" placeholder="Buscar" onkeyup="BuscarJ(this.value)">
                        </div>
                        <div id="procesosJ">
                            <?php include "logJefe.php"; ?>
                        </div>
                    </div>

                    <div class="tab-pane" id="recursos" role="tabpanel" aria-labelledby="deals-tab">
                        <div>
                            <input id="filtroR" type="text" class="form-control" placeholder="Buscar" onkeyup="BuscarR(this.value)">
                        </div>
                        <div id="procesosR">
                            <?php include "logRecursos.php"; ?>
                        </div>
                    </div>
                    <div class="tab-pane" id="entrevistador" role="tabpanel" aria-labelledby="deals-tab">
                        <div>
                            <input id="filtroE" type="text" class="form-control" placeholder="Buscar" onkeyup="BuscarE(this.value)">
                        </div>
                        <div id="procesosE">
                            <?php include "logEntre.php"; ?>
                        </div>
                    </div>
                    <div class="tab-pane" id="administrador" role="tabpanel" aria-labelledby="deals-tab">
                        <div>
                            <input id="filtroAd" type="text" class="form-control" placeholder="Buscar" onkeyup="BuscarAd(this.value)">
                        </div>
                        <div id="procesosAd">
                            <?php include "logAdmin.php"; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var c = 0;
        if (c == 0) {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/logAspirante.php") ?>";
            $("#procesosA").load(url);
            c = c + 1;
        }

        function BuscarAsp(val) {
            $(document).ready(function() {
                var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/logAspirante.php") ?>&filtro=" + val;
                $("#procesosA").load(url);
            })
        }

        function BuscarJ(val) {
            $(document).ready(function() {
                var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/logJefe.php") ?>&filtro=" + val;
                $("#procesosJ").load(url);
            })
        }

        function BuscarR(val) {
            $(document).ready(function() {
                var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/logRecursos.php") ?>&filtro=" + val;
                $("#procesosR").load(url);
            })
        }

        function BuscarE(val) {
            $(document).ready(function() {
                var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/logEntre.php") ?>&filtro=" + val;
                $("#procesosE").load(url);
            })
        }

        function BuscarAd(val) {
            $(document).ready(function() {
                var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/logAdmin.php") ?>&filtro=" + val;
                $("#procesosAd").load(url);
            })
        }

        $('#bologna-list a').on('click', function(e) {
            e.preventDefault()
            $(this).tab('show')
        })

        function editar(id, val, t) {
            var rol = "";
            var url2 = "";
            switch (t) {
                case 1: {
                    rol = "A";
                    url2 = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/aspirantesAjax.php") ?>&filtro=" + $("#filtroA").val() + "&pagina=" + $("#paginaAct").val();
                }
                break;
            case 2: {
                rol = "J";
                url2 = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/jefesAjax.php") ?>&filtro=" + $("#filtroJ").val() + "&pagina=" + $("#paginaActJ").val();
            }
            break;
            case 3: {
                rol = "R";
                url2 = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/recursosAjax.php") ?>&filtro=" + $("#filtroR").val() + "&pagina=" + $("#paginaActR").val();
            }
            break;
            case 4: {
                rol = "E";
                url2 = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/entrevistadorAjax.php") ?>&filtro=" + $("#filtroE").val() + "&pagina=" + $("#paginaActE").val();
            }
            break;
            case 5: {
                rol = "Ad";
                url2 = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/administradoresAjax.php") ?>&filtro=" + $("#filtroAd").val() + "&pagina=" + $("#paginaActAd").val();
            }
            break;
            }

            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/editar.php") ?>&edic=Estado&edit=" + val + "&rol=" + rol + "&id=" + id;
            $("#alerta").load(url);
            $('#cargando' + rol + id).show();
            setTimeout(function() {
                $("#procesos" + rol).load(url2);
            }, 500);

        }
    </script>