<?php
$admin = new Administrador($_GET["id"]);
$admin->consultar();

?>
<div class="modal-content b">
    <div class="modal-header b">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo $admin->getNombre() . " " . $admin->getApellido() ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <input id="cargo" type="hidden" value="">
    <div class="modal-body">
        <div class="row">
            <div class="col-4">
                <img src="<?php echo ($admin->getFoto() != "") ? $admin->getFoto() :
                                "http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user-setting-icon.png"; ?>" width="100%" class="img-thumbnail">
            </div>
            <div class="col-8">
                
                <div class="card border border-secondary" style="border-radius: 15px;">
                <h4 class="card-title text-center"><?php echo "Informacion básica" ?></h4>
                    <div class="m-2">
                        <p class="card-text"><b>Correo: </b><?php echo $admin->getCorreo() ?></p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">

    </div>
</div>