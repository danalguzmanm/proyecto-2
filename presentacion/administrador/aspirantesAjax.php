<?php
$cantidad = 5;
$pagina = 1;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$aspirante = new Aspirante();
$aspirantes = "";
if (isset($_GET["filtro"])) {
    $aspirantes = $aspirante->consultarPaginacionFiltro($cantidad, $pagina, $_GET["filtro"]);
} else {
    $aspirantes = $aspirante->consultarPaginacion($cantidad, $pagina);
}

$totalRegistros = count($aspirantes);

$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0 || $totalRegistros % $cantidad == 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>

<input id="id" type="hidden" value="<?php echo  $id ?>">
<input id="filtro" type="hidden" value="<?php echo  $_GET["filtro"] ?>">
<input id="paginaAct" type="hidden" value="<?php echo  $pagina ?>">
<div id="alerta"></div>
<div id="procesos" style="min-height: 300px;">
    <div class="text-right">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($aspirantes) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
    <br>
    <div class="table-responsive bg-white border border-secondary" style="border-radius: 8px;">
        <table class="table ">
            <thead>
                <tr class="text-center">
                    <th scope="col">Nombre</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Informacion</th>
                    <th scope="col">Habilitar/Deshabilitar</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($aspirantes as $Act) { ?>
                    <tr class="text-center">
                        <td><?php echo $Act->getNombre() . " " . $Act->getApellido() ?></td>
                        <td><?php echo $Act->getCorreo() ?></td>
                        <td><?php echo ($Act->getEstado() == 1) ? "Activo" : "Deshabilitado" ?></td>
                        <td><?php echo "<button id='" . $Act->getIdAspirante() . "' class='btn' onclick='modal(this.id)'><i class='fas fa-eye'></i></button>" ?></td>
                        <td><?php echo ($Act->getEstado() == 1) ? "<button id='" . $Act->getIdAspirante() . "' class='btn' value='2' onclick='editar(this.id,this.value,1)'><i class='fas fa-user-times'></i></i></button>" :
                                "<button id='" . $Act->getIdAspirante() . "' class='btn' value='1' onclick='editar(this.id,this.value,1)'><i class='fas fa-user-check'></i></i></button>" ?></td>
                        <td>
                            <div id='cargandoA<?php echo $Act->getIdAspirante(); ?>' class="spinner-border text-dark" role="status" style="display: none;">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-9">
        <nav>
            <ul class="pagination">
                <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><button class="page-link" onclick="anterior()"> &lt;&lt; </button></li>
                <?php
                for ($i = 1; $i <= $totalPaginas; $i++) {
                    if ($i == $pagina) {
                        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                    } else {
                        echo "<li class='page-item'><button class='page-link' onclick='elegirpage(this)'>" . $i . "</button></li>";
                    }
                }
                ?>
                <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><button class="page-link" onclick="siguiente()"> &gt;&gt; </button></li>
            </ul>
        </nav>
    </div>
    <div class="col-3 d-flex justify-content-end">
        <select id="cantidad" class="form-control" style="width: 60px;" onchange="cantidad(this.value)">
            <option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
            <option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
            <option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
            <option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
        </select>
    </div>
</div>
<!-- Modal información -->
<div class="modal fade mt-5 b" id="asp" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div id="modalinfoasp">

        </div>
    </div>
</div>

<div id="alerta"></div>

<script>
    function siguiente() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/aspirantesAjax.php") . "&pagina=" . ($pagina + 1); ?>";
        $("#procesos").load(url);
    }

    function elegirpage(obj) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/aspirantesAjax.php") ?>&pagina=" + $(obj).text();
        $("#procesos").load(url);
    }

    function anterior() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/aspirantesAjax.php") . "&pagina=" . ($pagina - 1); ?>";
        $("#procesos").load(url);
    }

    function cantidad(val) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/aspirantesAjax.php"); ?>" + "&cantidad=" + val;
        $("#procesos").load(url);
    }

    function modal(id) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/aspiranteModal.php"); ?>" + "&id=" + id;
        $("#modalinfoasp").load(url);
        $('#asp').modal('show');
    }
</script>