<?php
$log = new Log($_GET["id"]); 
$log->consultarE();

$actor = new Entrevistador($log->getActor());
$actor->consultar();
?>
<div class="modal-content b" style=" height: 82%;">
    <div class="modal-header b">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo $log->getNombre() ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <input id="cargo" type="hidden" value="">
    <div class="modal-body overflow-auto">
        <div class="row">
            <div class="col-12 col-lg-4 col-sm-4">
                <img src="<?php echo ($actor->getFoto() != "") ? $actor->getFoto() :
                                "http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user-setting-icon.png"; ?>" width="100%" class="img-thumbnail">
                <div class="text-center">
                    <br>
                    <p class="card-text"><b>Nombre: </b><?php echo $actor->getNombre() . " " . $actor->getApellido() ?></p>
                    <p class="card-text"><b>Correo: </b><?php echo $actor->getCorreo()  ?></p>
                </div>

            </div>
            <div class="col-12 col-lg-8 col-sm-8">

                <div class="card border border-secondary" style="border-radius: 15px;">
                    <h4 class="card-title text-center"><?php echo "Información sobre la acción" ?></h4>
                    <div class="m-2">
                        <p class="card-text"><b>Fecha: </b><?php echo $log->getFecha()  ?></p>
                        <p class="card-text"><b>Hora: </b><?php echo $log->getHora()  ?></p>
                        <?php
                        switch ($log->getIdLogAccion()) {
                            case '6': {
                                    $cargo = new Aspirante(explode(":", $log->getDatos())[0]);
                                    $cargo->consultar();
                        ?>
                                    <div class="card border border-secondary overflow-auto" style="border-radius: 15px; height: 280px;">
                                        <div class="m-2">
                                            <p class="card-text text-center"><b><?php echo "Entrevista" ?></b></p>
                                            <p class="card-text"><b>Aspirante evaluado: </b><?php echo $cargo->getNombre()." ".$cargo->getApellido()  ?></p>
                                            <p class="card-text"><b>Correo: </b><?php echo $cargo->getCorreo()  ?></p>
                                            <p class="card-text"><b>Puntaje: </b><?php echo explode(":", $log->getDatos())[1]  ?></p>
                                        </div>
                                    </div>

                                <?php
                                }
                                break;
                            case '3': {
                                ?>
                                    <div class="table-responsive" style="height: 250px;">
                                        <div class="table-responsive bg-white border border-secondary" style="border-radius: 8px;">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Apartado</th>
                                                        <th scope="col">Version Anterior</th>
                                                        <th scope="col">Nueva version</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $apartado = explode(" ", explode(".", explode("=", $log->getDatos())[0])[0])[3];
                                                    $anterior = explode("/", explode("=", $log->getDatos())[1]);
                                                    $nueva = array(
                                                        $actor->getNombre(),
                                                        $actor->getApellido(),
                                                        $actor->getCorreo(),

                                                    );
                                                    for ($i = 0; $i < 3; $i++) {

                                                        if (strcasecmp($apartado, explode(":", $anterior[$i])[0])) {
                                                            echo "<tr>";
                                                        } else {
                                                            echo "<tr class='bg-success'>";
                                                        }
                                                    ?>
                                                        <td scope="row"><?php echo explode(":", $anterior[$i])[0]; ?></td>
                                                        <td><?php echo explode(":", $anterior[$i])[1] ?></td>
                                                        <td><?php echo $nueva[$i] ?></td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                                <?php
                                }break;
                                    }
                                ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>