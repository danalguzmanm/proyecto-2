<?php
$cantidad = 5;
$pagina = 1;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$rec = new Administrador();
$recs = "";
if (isset($_GET["filtro"])) {
    $recs = $rec->consultarPaginacionFiltro($cantidad, $pagina, $_GET["filtro"]);
} else {
    $recs = $rec->consultarPaginacion($cantidad, $pagina);
}

$totalRegistros = count($recs);

$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0 || $totalRegistros % $cantidad == 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>

<input id="id" type="hidden" value="<?php echo  $id ?>">
<input id="filtro" type="hidden" value="<?php echo  $_GET["filtro"] ?>">
<input id="paginaActAd" type="hidden" value="<?php echo  $pagina ?>">
<div id="alerta"></div>
<div id="procesosAd" style="min-height: 300px;">
    <div class="text-right">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($recs) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
    <br>
    <div class="table-responsive bg-white border border-secondary" style="border-radius: 8px;">
        <table class="table ">
            <thead>
                <tr class="text-center">
                    <th scope="col">Nombre</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Informacion</th>
                    <th scope="col">Habilitar/Deshabilitar</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($recs as $Act) { ?>
                    <tr class="text-center">
                        <td><?php echo $Act->getNombre() . " " . $Act->getApellido() ?></td>
                        <td><?php echo $Act->getCorreo() ?></td>
                        <td><?php echo ($Act->getEstado() == 1) ? "Activo" : "Deshabilitado" ?></td>
                        <td><?php echo "<button id='" . $Act->getIdAdministrador() . "' class='btn' onclick='modalAd(this.id)'><i class='fas fa-eye'></i></button>" ?></td>
                        <td><?php echo ($Act->getEstado() == 1) ? "<button id='" . $Act->getIdAdministrador() . "' class='btn' value='2' onclick='editar(this.id,this.value,5)'><i class='fas fa-user-times'></i></i></button>" :
                                "<button id='" . $Act->getIdAdministrador() . "' class='btn' value='1' onclick='editar(this.id,this.value,5)'><i class='fas fa-user-check'></i></i></button>" ?></td>
                        <td>
                            <div id='cargandoAd<?php echo $Act->getIdAdministrador(); ?>' class="spinner-border text-dark" role="status" style="display: none;">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-9">
        <nav>
            <ul class="pagination">
                <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><button class="page-link" onclick="anterior()"> &lt;&lt; </button></li>
                <?php
                for ($i = 1; $i <= $totalPaginas; $i++) {
                    if ($i == $pagina) {
                        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                    } else {
                        echo "<li class='page-item'><button class='page-link' onclick='elegirpage(this)'>" . $i . "</button></li>";
                    }
                }
                ?>
                <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><button class="page-link" onclick="siguiente()"> &gt;&gt; </button></li>
            </ul>
        </nav>
    </div>
    <div class="col-3 d-flex justify-content-end">
        <select id="cantidad" class="form-control" style="width: 60px;" onchange="cantidad(this.value)">
            <option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
            <option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
            <option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
            <option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
        </select>
    </div>
</div>
<!-- Modal información -->
<div class="modal fade mt-5 b" id="admin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div id="modalinfoAd">

        </div>
    </div>
</div>

<div id="alerta"></div>

<script>
    function siguiente() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/administradorAjax.php") . "&pagina=" . ($pagina + 1); ?>";
        $("#procesosAd").load(url);
    }

    function elegirpage(obj) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/administradorAjax.php") ?>&pagina=" + $(obj).text();
        $("#procesosAd").load(url);
    }

    function anterior() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/administradorAjax.php") . "&pagina=" . ($pagina - 1); ?>";
        $("#procesosAd").load(url);
    }

    function cantidad(val) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/administradorAjax.php"); ?>" + "&cantidad=" + val;
        $("#procesosAd").load(url);
    }

    function modalAd(id) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/administradorModal.php"); ?>" + "&id=" + id;
        $("#modalinfoAd").load(url);
        $('#admin').modal('show');
    }
</script>