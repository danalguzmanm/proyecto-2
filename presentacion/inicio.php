<head>
  <link href="Style/design.css" rel="stylesheet">
  <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
</head>
<html>

<body class="fondo">
  <div id="carouselExampleIndicators" class=" carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active"> 
        <img src="img/imagen1.jpg" class="d-block w-100">
        <div class="info">
          <h2>!Punto&Coma!</h2>
          <p class="lead">Tu mejor opcion !</p>
       <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("presentacion/inicioPc.php") ?>">Ir al Inicio</a>
         </div>
      </div>
    </div>
  </div>

  <body class="fondo">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="img/imagenFoto.jpeg" class="d-block w-100" alt="...">
        <div class="info ">
          <h2>!Punto&Coma!</h2>
          <p class="lead">Less is more!</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/inicio.jpg" class="d-block w-100" alt="...">
        <div class="info">
          <h2>!Punto&Coma!</h2>
          <p class="lead">Empleo a la mano...</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/imagen1.jpg" class="d-block w-100" alt="...">
        <div class="info">
          <h2>!Punto&Coma!</h2>
          <p class="lead">No esperes mas!</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  </body>
  <div class="dropdown-divider"></div>

<!-- footer -->
<footer class="footer text-center py-x">
    
    <div class="col-md-12 text-center">
      <h3 class="text-white">Mas informacion en:</h3>
      <p class="lead">
        <i class="fas fa-envelope-square"></i> notenemoscorreo@jum.com <br>
        <i class="fas fa-phone-square-alt"></i> Imbox ;) <br>
        <i class="fas fa-concierge-bell"></i> Solo cuando toque  <br>
        <a href="index.php"><i class="fab fa-facebook-square"></i></a>
        <a href="index.php"><i class="fab fa-instagram"></i></a>
      </p>
    </div>

 
  <div class="container-fluid" id="footerDown">
    
    <p class="text-center">Punto&Com &copy; No copeo</p>
  </div>
  
</footer>
  
</body>
<!-- Modal -->
<div class="modal fade mt-5" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="exampleModalLabel">Inicio sesion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php") ?>" method="post">
                    <div class="form-group">
                        <input name="correo" type="email" class="form-control" placeholder="Correo" required>
                    </div>
                    <div class="form-group">
                        <input name="clave" type="password" class="form-control" placeholder="Clave" required>
                    </div>
                    <div class="form-group">
                        <input name="ingresar" type="submit" class="form-control btn bg-dark btn-dark">
                    </div>
                    <?php
                    if (isset($_GET["error"]) && $_GET["error"] == 1) {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Error de correo o clave</div>";
                    } else if (isset($_GET["error"]) && $_GET["error"] == 2) {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta no ha sido activada</div>";
                    } else if (isset($_GET["error"]) && $_GET["error"] == 3) {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>";
                    }
                    ?>
                </form>
                <p>¿Eres nuevo? <a href="index.php?pid=<?php echo base64_encode("presentacion/aspirante/registrarAspirante.php") ?>">Registrate</a></p>
            </div>
        </div>
    </div>
</div>

<?php
if (isset($_GET["error"])) {
?>
    <script>
        $('#exampleModal').modal('show');
    </script>
<?php
}
?>
<!--  -->