<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$entrevistador = new Entrevistador("", "", "", $correo, $clave);
$jefe = new Jefe("", "", "", $correo, $clave);
$aspirante = new Aspirante("", "", "", $correo, $clave);
$recursos = new Recursos("", "", "", $correo, $clave);
$fecha = new DateTime();
$ident = $fecha ->getTimestamp();
if ($administrador->autenticar()) {
    if ($administrador->getEstado() == "1") {
        $log = new Log($ident,"","","","1");
        $log ->insertar();
        $log ->asociarAd($administrador->getIdAdministrador());
        $_SESSION["rol"] = "Administrador";
        $_SESSION["id"] = $administrador->getIdadministrador();  /* se almacena la sesion  */
        header("Location: index.php?pid=" . base64_encode("presentacion/administrador/sesionAdministrador.php"));
    } else {
        header("Location: index.php?error=3");
    }
} else if ($entrevistador->autenticar()) {
    if ($entrevistador->getEstado() == "1") {
        $log = new Log($ident,"","","","1");
        $log ->insertar();
        $log ->asociarE($entrevistador->getIdEntrevistador());
        $_SESSION["rol"] = "Entrevistador";
        $_SESSION["id"] = $entrevistador->getIdEntrevistador();  /* se almacena la sesion  */
        header("Location: index.php?pid=" . base64_encode("presentacion/entrevistador/sesionEntrevistador.php"));
    } else {
        header("Location: index.php?error=3");
    }
} else if ($jefe->autenticar()) {
    if ($jefe->getEstado() == "1") {
        $log = new Log($ident,"","","","1");
        $log ->insertar();
        $log ->asociarJ($jefe->getIdJefe());
        $_SESSION["rol"] = "Jefe";
        $_SESSION["id"] = $jefe->getIdJefe();  /* se almacena la sesion  */
        header("Location: index.php?pid=" . base64_encode("presentacion/jefe/procesosJefe.php"));
    } else {
        header("Location: index.php?error=3");
    }
} else if ($aspirante->autenticar()) {
    if ($aspirante->getEstado() == "1") {
        $log = new Log($ident,"","","","1");
        $log ->insertar();
        $log ->asociarA($aspirante->getIdAspirante());
        $_SESSION["id"] = $aspirante->getIdAspirante();
        $_SESSION["rol"] = "Aspirante";
        header("Location: index.php?pid=" . base64_encode("Presentacion/Aspirante/sesionAspirante.php"));
    } else {
        header("Location: index.php?error=3");
    }
} else if ($recursos->autenticar()) {
    if ($recursos->getEstado() == "1") {
        $log = new Log($ident,"","","","1");
        $log ->insertar();
        $log ->asociarR($recursos->getIdRecursos());
        $_SESSION["id"] = $recursos->getIdRecursos();
        $_SESSION["rol"] = "Recursos";
        header("Location: index.php?pid=" . base64_encode("Presentacion/recursos/sesionRecursos.php"));
    } else {
        header("Location: index.php?error=3");
    }
} else {
    header("Location: index.php?error=1");
}
