<?php

$rec = new Aspirante();
$cantidad = 8;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$recursos="";
$totalRegistros="";
if (isset($_GET["id"])) {
    $recursos = $rec->consultarPaginacionRec($cantidad, $pagina, $_GET["id"]);
    $totalRegistros = count($recursos);
} else {
    $recursos = $rec->consultarPaginacion($cantidad, $pagina);
    $totalRegistros = $rec->consultarCantidad();
}

$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);

if( isset ($_GET["t"])){
    $fecha = new DateTime();
    $id = $fecha->getTimestamp();
    $ident = $fecha ->getTimestamp();
    $obj = new Encuesta($id,$_GET["id2"],$_GET["t"],$_GET["p"],$_SESSION["id"]);
    $obj->eliminar();
    $obj->insertar();
    $log = new Log($ident,"","",$_GET["id2"].":".$_GET["p"],"6");
    $log ->insertar();
    $log ->asociarE($entrevistador->getIdEntrevistador());
    $aspirante = new Aspirante($_GET["id2"]);
    $aspirante ->consultar();
    $aspc = new CargoAspirante("",$aspirante->getIdAspirante());
    $aux=$aspc->consultarTodosA();
    foreach($aux as $auxAct){
        $contra = new Contratacion("","",$auxAct->geIdCargo());
        $contra ->consultar();
        $act = new Actividad("", "", "evaluó al aspirante ".$aspirante->getNombre()." ".$aspirante->getApellido()
        ." otorgandole un puntaje de ".$_GET["p"], $contra->getIdContratacion(),$_SESSION["id"], "Entrevistador");
        $act->insertar();
    }
}

?>

<body style="background-image: url(img/selecEnt.jpg);
 background-size:cover;">

</body>
<div class="container mt-3">

    <div class="row">
        <div class="col">
            <div class="card">
            <div class="card-header text-white text-center bg-dark">
            <h3 class="text-center text-white"><i class="fa fa-list-alt"></i> Listado de Aspirantes</h3>
				</div>
                <div class="container text-left lead">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($recursos) ?> de <?php echo $totalRegistros ?> Aspirantes registrados</div>
                <div class="card-body">
                    <div class="row">
                        <?php foreach ($recursos as $recursoActual) { ?>

                            <div class="col-3">

                                <div class="card">
                                    <img title="<?php echo $recursoActual->getNombre(); ?>" alt="<?php echo $recursoActual->getNombre(); ?>" class="card-img-top" src="<?php echo ($recursoActual->getfoto()) != "" ? $recursoActual->getfoto() : "img/siempre/predeterminada.png"; ?>" data-toggle="gggg" data-trigger="hover" data-content="<?php echo $recursoActual->getDescripcion() ?>" height="200px">

                                    <div class="card-body ">
                                        <span><?php echo $recursoActual->getNombre(); ?></span>
                                        <span><?php echo $recursoActual->getApellido(); ?></span>
                                        <form action="" method="post">

                                            <input type="hidden" name="id" id="id" value="<?php echo $recursoActual->getIdAspirante(); ?>">
                                            <input type="hidden" name="nombre" id="nombre" value="<?php echo $recursoActual->getNombre(); ?>">
                                            <input type="hidden" name="apelldio" id="apellido" value="<?php echo $recursoActual->getApellido(); ?>">

                                        </form>
                                        <?php echo "<button id='" . $recursoActual->getIdAspirante() . "' class='btn btn-dark rounded-pill' onclick='modal(this.id)'><i class='fas fa-info-circle'></i></button>" ?>

                                    </div>
                                </div>

                            </div>

                        <?php } ?>

                    </div>

                </div>
                <div class="card-footer">
                    <div class="text-center">
                        <nav>
                            <ul class="pagination">
                                <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/recursos/listAsp.php") . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                                <?php
                                for ($i = 1; $i <= $totalPaginas; $i++) {
                                    if ($i == $pagina) {
                                        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                                    } else {
                                        echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/recursos/listAsp.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                                    }
                                }
                                ?>
                                <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/recursos/listAsp.php") . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal información -->
<div class="modal fade mt-5" id="info" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="exampleModalLabel">Información</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalinfo">

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function modal(id) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/encuesta/entrevista.php"); ?>&idas=" + id;
        $("#modalinfo").load(url);
        $('#info').modal('show');
    }
    $(function() {
        $('[data-toggle="gggg"]').popover()
    });
    $("#cantidad").on("change", function() {
        url = "index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoPagina.php") ?>&cantidad=" + $(this).val();
        location.replace(url);
    });
</script>