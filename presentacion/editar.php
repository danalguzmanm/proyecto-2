<?php
$fecha = new DateTime();
$ident = $fecha->getTimestamp();
$tipo = "";
if(isset($_GET["rol"])){
if ($_GET["rol"] == "E") {
  $tipo = "Entrevistador";
  $actor = new Entrevistador($_GET["id"]);
  $actor->consultar();
  $actor->editar($_GET["edic"], $_GET["edit"]);
  if ($_GET["edic"] != "Estado") {
    $log = new Log($ident, "", "", "Editó el apartado " . $_GET["edic"] . ". Informacion antes de la edición="
      . "Nombre: " . $actor->getNombre() . "/Apellido: " . $actor->getApellido()
      . "/Correo: " . $actor->getCorreo() . "", "3");
    $log->insertar();
    $log->asociarE($actor->getIdEntrevistador());
  }
} else if ($_GET["rol"] == "J") {
  $tipo = "Jefe";
  $actor = new Jefe($_GET["id"]);
  $actor->consultar();
  $actor->editar($_GET["edic"], $_GET["edit"]);
  if ($_GET["edic"] != "Estado") {
    $log = new Log($ident, "", "", "Editó el apartado " . $_GET["edic"] . ". Informacion antes de la edición="
      . "Nombre: " . $actor->getNombre() . "/Apellido: " . $actor->getApellido()
      . "/Correo: " . $actor->getCorreo() . "", "3");
    $log->insertar();
    $log->asociarJ($actor->getIdJefe());
  }
} else if ($_GET["rol"] == "A") {
  $tipo = "Aspirante";
  $actor = new Aspirante($_GET["id"]);
  $actor->consultar();
  $actor->editar($_GET["edic"], $_GET["edit"]);
  if ($_GET["edic"] != "Estado") {
    $log = new Log($ident, "", "", "Editó el apartado " . $_GET["edic"] . ". Informacion antes de la edición="
      . "Nombre: " . $actor->getNombre() . "/Apellido: " . $actor->getApellido() . "/Edad:" . $actor->getEdad()
      . "/Correo: " . $actor->getCorreo() . "/Sexo: " . $actor->getSexo() . "/Civil: " . $actor->geTCivil()
      . "/Descripción: " . $actor->getDescripcion() . "", "3");
    $log->insertar();
    $log->asociarA($actor->getIdAspirante());
  }
} else if ($_GET["rol"] == "Ad") {
  $tipo = "Administrador";
  $actor = new Administrador($_GET["id"]);
  $actor->consultar();
  $actor->editar($_GET["edic"], $_GET["edit"]);
  if ($_GET["edic"] != "Estado") {
    $log = new Log($ident, "", "", "Editó el apartado " . $_GET["edic"] . ". Informacion antes de la edición="
      . "Nombre: " . $actor->getNombre() . "/Apellido: " . $actor->getApellido()
      . "/Correo: " . $actor->getCorreo() . "", "3");
    $log->insertar();
    $log->asociarAd($actor->getIdAdministrador());
  }
} else if ($_GET["rol"] == "R") {
  $tipo = "Recursos";
  $actor = new Recursos($_GET["id"]);
  $actor->consultar();
  $actor->editar($_GET["edic"], $_GET["edit"]);
  if ($_GET["edic"] != "Estado") {
    $log = new Log($ident, "", "", "Editó el apartado " . $_GET["edic"] . ". Informacion antes de la edición="
      . "Nombre: " . $actor->getNombre() . "/Apellido: " . $actor->getApellido()
      . "/Correo: " . $actor->getCorreo() . "", "3");
    $log->insertar();
    $log->asociarR($actor->getIdRecursos());
  }
}
}
if (isset($_GET["admin"])) {
  $log = new Log($ident, "", "", "Modificó el estado del :".$tipo." :".$actor->getEstado(), "7");
  $log->insertar();
  $log->asociarAd($_GET["admin"]);
}
if (isset($_GET["vacantes"])) {
  $vac = new Cargo($_GET["vacantes"]);
  $vac->consultar();
  $vac->editar("Vacantes",$vac->getVacantes()-1);

  $cont = new Contratacion("","",$vac->geIdCargo());
  $cont->consultar();

  $act = new Actividad("","","contratacion del Aspirante",$cont->getIdContratacion(),$cont->getIdRecursos(),"recursos");
  $act->insertar();
  if(($vac->getVacantes()-1) == 0 ){
    $vac->editar("Estado","finalizado");
    $act = new Actividad("","","finalizacion del proceso",$cont->getIdContratacion(),$cont->getIdRecursos(),"recursos");
    $act->insertar();
  }

}
?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
		swal("¡Hecho!","Proceso realizado", "success");
		//Puedes colocar warning, error, success y por último info.
	</script> 
