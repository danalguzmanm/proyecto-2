<?php
$cantidad = 5;
$pagina = 1;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
if (isset($_GET["cant"])) {
    $fecha = new DateTime();
    $codigo = $fecha->getTimestamp();
    $ident = $fecha->getTimestamp();
    $nuevo = new Cargo($codigo, $_GET["nom"], $_GET["cant"], $_GET["fe"], $_GET["id"], "", str_replace("/", " ", $_GET["desc"]));
    $nuevo->insertar();
    $log = new Log($ident, "", "","Creacion de proceso cargo: ".$codigo, "5");
    $log->insertar();
    $log->asociarJ( $_GET["id"]);
    $rec = new Recursos();
    $recs = $rec->consultarTodosId();
    $rand = rand(0, count($recs) - 1);
    $codrec = $recs[$rand]->getIdRecursos();
    $contra = new Contratacion("", $codrec, $codigo);
    $contra->insertar();
    $contra->consultar();
    $act = new Actividad("", "", "Inicio del proceso", $contra->getIdContratacion(), 1, "Jefe");
    $act->insertar(); 
}
$id = "";

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $cargo = new Cargo("", "", "", "", $_GET["id"]);
} else {
    $id = $_SESSION["id"];
    $cargo = new Cargo("", "", "", "", $_SESSION["id"]);
}
$totalRegistros = "";
if (isset($_GET["filtro"])) {
    $cant = $cargo->consultarCantidadFiltroJ($_GET["filtro"]);
    $cargos = $cargo->consultarPaginacionFiltroJ($cantidad, $pagina, $_GET["filtro"]);
    $totalRegistros = $cant;
} else {
    $cant = $cargo->consultarJefe();
    $cargos = $cargo->consultarPaginacionJefe($cantidad, $pagina);
    $totalRegistros = count($cant);
}

$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0 || $totalRegistros % $cantidad == 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>

<input id="id" type="hidden" value="<?php echo  $id ?>">
<div id="procesos" style="min-height: 300px;">
    <div class="text-right">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($cargos) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
    <br>
    <div class="table-responsive bg-white border border-secondary" style="border-radius: 8px;">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Cargo</th>
                    <th scope="col">Vacantes</th>
                    <th scope="col">Fecha limite</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Información</th>
                    <th scope="col">Documentación</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($cargos as $cargoAct) { ?>
                    <tr>
                        <td><?php echo $cargoAct->getNombre() ?></td>
                        <td><?php echo $cargoAct->getVacantes() ?></td>
                        <td><?php echo $cargoAct->getFecha() ?></td>
                        <td><?php echo $cargoAct->getEstadp() ?></td>
                        <td><?php echo "<button id='" . $cargoAct->geIdCargo() . "' class='btn btn-dark rounded-pill' onclick='modal(this.id)'><i class='fas fa-info-circle'></i></button>" ?></td>
                        <td><?php echo "<a class='btn btn-dark rounded-pill' target='_blank' href='index.php?pid=" . base64_encode("presentacion/documentacion.php") . "&id=" . $cargoAct->geIdCargo() . "' role='button'><i class='fas fa-file-alt'></i></i></a>" ?></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-9">
        <nav>
            <ul class="pagination">
                <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><button class="page-link" onclick="anterior()"> &lt;&lt; </button></li>
                <?php
                for ($i = 1; $i <= $totalPaginas; $i++) {
                    if ($i == $pagina) {
                        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                    } else {
                        echo "<li class='page-item'><button class='page-link' onclick='elegirpage(this)'>" . $i . "</button></li>";
                    }
                }
                ?>
                <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><button class="page-link" onclick="siguiente()"> &gt;&gt; </button></li>
            </ul>
        </nav>
    </div>
    <div class="col-3 d-flex justify-content-end">
        <select id="cantidad" class="form-control" style="width: 60px;" onchange="cantidad(this.value)">
            <option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
            <option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
            <option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
            <option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
        </select>
    </div>
</div>
<!-- Modal información -->
<div class="modal fade mt-5" id="info" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="exampleModalLabel">Información</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalinfo">

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function siguiente() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/jefe/procesosAjax.php") . "&pagina=" . ($pagina + 1); ?>" + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function elegirpage(obj) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/jefe/procesosAjax.php") ?>&pagina=" + $(obj).text() + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function anterior() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/jefe/procesosAjax.php") . "&pagina=" . ($pagina - 1); ?>" + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function cantidad(val) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/jefe/procesosAjax.php"); ?>" + "&cantidad=" + val + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function modal(id) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/jefe/cargo.php"); ?>&id=" + id;
        $("#modalinfo").load(url);
        $('#info').modal('show');
    }
</script>