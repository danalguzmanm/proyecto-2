<?php
$cargo = new Cargo($_GET["id"]);
$cargo->consultar();
$jefe = new Jefe($cargo->getIdJefe());
$jefe->consultar();
?>
<div>
    <div class="row">
        <div class="col-4">
            <img src="<?php echo ($jefe->getFoto() != "") ? $jefe->getFoto() :
                            "http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user-setting-icon.png"; ?>" width="100%" class="img-thumbnail">
            <p class="text-center"><b><?php echo $jefe->getNombre()." ". $jefe->getApellido() ?></b></p>
        </div>
        <div class="col-8">
            <p><b>Cargo: </b><?php echo $cargo->getNombre() ?></p>
            <p><b>Vacantes: </b><?php echo $cargo->getVacantes() ?></p>
            <p><b>Estado: </b><?php echo $cargo->getEstadp() ?></p>
            <p><b>Descripción: </b><?php echo $cargo->getDescripcion() ?></p>
            <p><b>Fecha liminte de postulacion: </b><?php echo $cargo->getFecha() ?></p>
        </div>
    </div>
</div>