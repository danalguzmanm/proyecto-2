<body style="background-image: url(img/fondoPerfil.jpg);
background-size: cover">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <div class="container mt-5">
        <div class="card f">
            <div class="card-header bg-dark text-white text-center">
                <h4>Tus procesos</h4>
            </div>
            <div class="card-body f" style="min-height: 300px;">
                <div>
                    <input type="text" class="form-control" placeholder="Buscar" onkeyup="Buscar(this.value)">
                </div>
                <div id="procesos">
                    <?php include "procesosAjax.php"; ?>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end">

                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#agregarp">
                    <i class="fas fa-address-card"></i> Nuevo proceso
                </button>
            </div>
        </div>
    </div>

    <!-- Modal agregar proceso-->
    <div class="modal fade mt-5" id="agregarp" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    NOMBRE DEL CARGO
                    <input type="text" class="form-control" id="nombre" required>
                    FECHA LIMITE PARA ASPIRAR
                    <input type="date" class="form-control" id="fecha" required>
                    NUMERO DE VACANTES
                    <input type="number" class="form-control" value="1" min="1" max="10" id="cantidadv" required>
                    DESCRIPCION
                    <textarea class="form-control" id="desc" required></textarea>
                    <input type="hidden" id="id" value="<?php echo $_SESSION["id"]; ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="agregar()">Agregar</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function agregar() {
            swal("¡Hecho!", "Proceso realizado", "success");
            var f = $("#desc").val();
            $(document).ready(function() {
                var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/jefe/procesosAjax.php") ?>&nom=" + $("#nombre").val() + "&fe=" + $("#fecha").val() + "&cant=" + $("#cantidadv").val() + "&id=" + $("#id").val() + "&desc=" + f.split(" ").join("/");
                $("#procesos").load(url);
            })
            $('#agregarp').modal('hide');
        }

        function Buscar(val) {
            setTimeout(function() {
                $(document).ready(function() {
                    var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/jefe/procesosAjax.php") ?>&filtro=" + val + "&id=" + $("#id").val();
                    $("#procesos").load(url);
                })
            }, 500);

        }
    </script>