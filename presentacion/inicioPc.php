<html>
<head>
  <link href="Style/design.css" rel="stylesheet">
  <!-- <link href="css/estilos.css" rel="stylesheet"> -->
</head>
<body style="background-image: url(img/inicio.jpg);
background-size: cover">
 <?php 
 include "presentacion/menuInicio.php"; 
 ?>
  <!-- INFO DE INICIO -->
  <div class="f container bg-white">

    <div class="f card-header bg-light mt-5 ">
      <h1 class="text-center mt-5">AL LADO DE QUIEN NOS NECESITA</h1>
    </div>
    <div class="f card-body bg-ligth">
      <div class="row text-justify ">


        <div class="col-md-6">
          <p class="text-dark lead">
            <span style="color:brown">Punto&Coma</span> Ofrecemos servicios de Outsourcing de Nómina, Gestión de Personal y Outsourcing de Empleo (PEO). Estamos orientados a brindar soluciones integrales y flexibles, generando valor estratégico en nuestros clientes. Tenemos amplia experiencia atendiendo empresas extranjeras, locales y de múltiples sectores económicos.</p>
        </div>

        <div class="col-md-6">
          <p class="text-dark lead">
          Nuestro servicio de atención e información se asienta sobre un equipo profesional altamente cualificado, pero, sobre todo, en un equipo empático y cercano, capaz de ponerse en el lugar de quien precisa de ayuda u orientación en un momento puntual.
          </p>
        </div>
        

      </div>
    </div>
    <div class="dropdown-divider"></div>
    <div class="f container mt-3">
      <div class="f card-body bg-dark rounded-lg contenido-container">

        <div class="row">

          <div class="col-md-6 ml-auto ">
            <img src="img/inicio4.jpg" class="img-fluid  rounded-lg">
          </div>

          <div class="col-md-6 text-center text-dark">
            <div class="f card-body bg-light rounded-lg">
              <h1>Punto&Coma</h1>
              <p class="lead">
              <br><br>
                - Atención e información a usuarios<br>
                - Servicios de atención a la ciudadanía.<br>
                - Información previa de nuestros servicios.<br>
                - Atención y asistencia a personas en desempleo.
              </p>
              <br><br><br>
            </div>


          </div>
        </div>
      </div>
    </div>