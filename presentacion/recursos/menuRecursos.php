<?php
$aspirante = new Recursos($_SESSION["id"]);
$aspirante->consultar();
?>
<head>
	<link href="Style/design.css" rel="stylesheet">
</head>

<body class="fondo">
	<nav class="navbar navbar-expand-lg ">
		<div class="container text-dark">
			<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/recursos/sesionRecursos.php") ?>">
			<strong class="text-white"><img src="img/logo.png"class="mb-5" style="height: 80px;"><span class="display-4 "> Recursos </span></strong></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				
				<ul class="navbar-nav ml-auto text-white">
					<a class="nav-link text-white" href="index.php?pid=<?php echo base64_encode("Presentacion/inicio2.php") ?>"><i class="fas fa-home"></i> Inicio</a>
					<a class="nav-link text-white" href="index.php?pid=<?php echo base64_encode("Presentacion/recursos/listaAsp.php") ?>"><i class="fa fa-list-alt"></i> Lista Aspirantes</a>
					<a class="nav-link text-white" href="index.php?pid=<?php echo base64_encode("Presentacion/recursos/procesosRecursos.php") ?>"><i class="fa fa-list-alt"></i> Mis procesos </a>
					<li class="nav-item dropdown active "><a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-user-circle" ></i> <?php echo ($aspirante->getNombre() != "" ? $aspirante->getNombre() : $aspirante->getCorreo()) ?> <?php echo $aspirante->getApellido() ?></a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item " href="index.php?pid=<?php echo base64_encode("Presentacion/recursos/perfilRecursos.php") ?>"><i class="fas fa-user"></i> Ver perfil</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="index.php?cerrarSesion=true"><i class="fas fa-sign-out-alt"></i> Cerrar Sesion</a>
						</div>
					</li>
				</ul>
			</div>
</body>

</nav>