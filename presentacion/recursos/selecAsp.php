<?php
$selec = new Aspirante($_GET["id"]);
$selec->consultar();
$Rec = new Aspirante($selec->getIdAspirante());
$Rec->consultar();
$enc = new Encuesta("",$_GET["id"]);
$enc1 = $enc->consultarPunt();
$puntaje = "Sin entrevistar";
foreach($enc1 as $encactual){
if($encactual->getTipo()=="entrevista"){
$puntaje=$encactual->getPuntaje();
}
}
$car = new Cargo($_GET["idset"]);
$car->consultar();

?>
<div>
    <div class="row">
        <div class="col-4">
            <img src="<?php echo ($Rec->getFoto() != "") ? $Rec->getFoto() :
                            "http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user-setting-icon.png"; ?>" width="100%" class="img-thumbnail">
            <p class="text-center"><b><?php echo $Rec->getNombre()." ". $Rec->getApellido() ?></b></p>
        </div>
        <div class="col-8">
            <p><b>Nombre: </b><?php echo $selec->getNombre() ?></p>
            <p><b>Apellido: </b><?php echo $selec->getApellido() ?></p>
            <p><b>Edad: </b><?php echo $selec->getEdad() ?></p>
            <p><b>Estado Civil: </b><?php echo $selec->geTCivil() ?></p>
            <p><b>Descripcion: </b><?php echo $selec->getDescripcion() ?></p>
            <p><b>Puntaje Entrevistador: </b><?php echo $puntaje?></p>
            
        </div>
    </div>
<div class="card-footer" >
    <?php 
    if($car->getVacantes()== 0 ){
        echo"Sin capacidad de vacantes";
    }else{

    ?>

<button type="button" id="<?php echo $_GET["idset"] ?>" class="btn btn-dark" onclick="Contratar(this.id)">Contratar</button>
<?php 
}
?>
</div>
</div>
<div id="alerta" name="alerta"></div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
function Contratar(id){
     $(document).ready(function() {
				var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/editar.php") ?>"+ "&vacantes=" + id;
				$("#alerta").load(url);
			})
	swal("¡Hecho!", " Se contrato al aspirante ", "success");
		//Puedes colocar warning, error, success y por último info.
}
</script>