<?php
$cantidad = 5;
$pagina = 1;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$id = "";

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $contratacion = new Contratacion("", $_GET["id"]);
} else {
    $id = $_SESSION["id"];
    $contratacion = new Contratacion("", $_SESSION["id"]);
}
$totalRegistros = "";
if (isset($_GET["filtro"])) {
    $aux = new Cargo();
    $contrataciones = $aux->consultarPaginacionFiltroRec($cantidad, $pagina, $_GET["filtro"], $contratacion->getIdRecursos());
    $totalRegistros = count($contrataciones);
} else {
    $cant = "";
    $contrataciones = $contratacion->consultarPaginacionRec($cantidad, $pagina);
    $totalRegistros = count($contrataciones);
}

$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0 || $totalRegistros % $cantidad == 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>

<input id="id" type="hidden" value="<?php echo  $id ?>">
<div id="procesos" style="min-height: 300px;">
    <div class="text-right">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($contrataciones) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
    <br>
    <div class="table-responsive bg-white border border-secondary" style="border-radius: 8px;">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Cargo</th>
                    <th scope="col">Vacantes</th>
                    <th scope="col">Fecha limite</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Información</th>
                    <th scope="col">Seguimiento</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($_GET["filtro"])) {
                    foreach ($contrataciones as $contraActual) {
                ?>
                        <tr>
                            <td><?php echo $contraActual->getNombre() ?></td>
                            <td><?php echo $contraActual->getVacantes() ?></td>
                            <td><?php echo $contraActual->getFecha() ?></td>
                            <td><?php echo $contraActual->getEstadp() ?></td>
                            <td><?php echo "<button id='" . $contraActual->geIdCargo() . "' class='btn btn-dark rounded-pill' onclick='modal(this.id)'><i class='fas fa-info-circle'></i></button>" ?></td>
                            <td><?php echo "<a class='btn btn-dark rounded-pill' href='index.php?pid=" . base64_encode("presentacion/recursos/seguimiento.php") . "&id=" . $contraActual->geIdCargo() . "' role='button'><i class='fas fa-file-alt'></i></i></a>" ?></td>
                        </tr>
                    <?php
                    }
                } else {
                    foreach ($contrataciones as $contraActual) {
                        $cargo = new Cargo($contraActual->geIdCargo());
                        $cargo->consultar();
                    ?>
                        <tr>
                            <td><?php echo $cargo->getNombre() ?></td>
                            <td><?php echo $cargo->getVacantes() ?></td>
                            <td><?php echo $cargo->getFecha() ?></td>
                            <td><?php echo $cargo->getEstadp() ?></td>
                            <td><?php echo "<button id='" . $cargo->geIdCargo() . "' class='btn btn-dark rounded-pill' onclick='modal(this.id)'><i class='fas fa-info-circle'></i></button>" ?></td>
                            <td><?php echo "<a class='btn btn-dark rounded-pill' href='index.php?pid=" . base64_encode("presentacion/recursos/seguimiento.php") . "&id=" . $contraActual->geIdCargo() . "' role='button'><i class='fas fa-file-alt'></i></i></a>" ?></td>
                        </tr>
                <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-9">
        <nav>
            <ul class="pagination">
                <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><button class="page-link" onclick="anterior()"> &lt;&lt; </button></li>
                <?php
                for ($i = 1; $i <= $totalPaginas; $i++) {
                    if ($i == $pagina) {
                        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                    } else {
                        echo "<li class='page-item'><button class='page-link' onclick='elegirpage(this)'>" . $i . "</button></li>";
                    }
                }
                ?>
                <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><button class="page-link" onclick="siguiente()"> &gt;&gt; </button></li>
            </ul>
        </nav>
    </div>
    <div class="col-3 d-flex justify-content-end">
        <select id="cantidad" class="form-control" style="width: 60px;" onchange="cantidad(this.value)">
            <option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
            <option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
            <option value="15" <?php echo ($cantidad == 15) ? "selected" : "" ?>>15</option>
            <option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
        </select>
    </div>
</div>
<!-- Modal información -->
<div class="modal fade mt-5" id="info" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="exampleModalLabel">Información</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalinfo">

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function siguiente() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/recursos/procesosAjax.php") . "&pagina=" . ($pagina + 1); ?>" + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function elegirpage(obj) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/recursos/procesosAjax.php") ?>&pagina=" + $(obj).text() + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function anterior() {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/recursos/procesosAjax.php") . "&pagina=" . ($pagina - 1); ?>" + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function cantidad(val) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/recursos/procesosAjax.php"); ?>" + "&cantidad=" + val + "&id=" + $("#id").val();
        $("#procesos").load(url);
    }

    function modal(id) {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/jefe/cargo.php"); ?>&id=" + id;
        $("#modalinfo").load(url);
        $('#info').modal('show');
    }
</script>