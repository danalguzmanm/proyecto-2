<body style="background-image: url(img/fondoPerfil.jpg);
background-size: cover">

    <div class="container mt-5 mb-5">
        <div class="card f">
            <div class="card-header bg-dark text-white text-center">
            <h3 class="text-center text-white"><i class="fa fa-battery-three-quarters"></i> Tus procesos</h3>
            </div>
            <div class="card-body f" style="min-height: 300px;">
                <div>
                    <input type="text" class="form-control" placeholder="Buscar" onkeyup="Buscar(this.value)">
                </div>
                <div id="procesos">
                    <?php include "procesosAjax.php"; ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        function Buscar(val){
            $(document).ready(function() {
                var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/recursos/procesosAjax.php") ?>&filtro="+val+ "&id=" + $("#id").val();
                $("#procesos").load(url);
            })
        }
    </script>