<?php
$idCargo="";
if (isset($_GET["id"])) {
    $cargo = new Cargo($_GET["id"]);
    $cargo->consultar();
    $jefe = new Jefe($cargo->getIdJefe());
    $jefe->consultar();
    $cargoasp = new CargoAspirante($cargo->geIdCargo());
    $aspirantes = $cargoasp->consultarTodos();
    $idCargo = $cargo->geIdCargo();
}
?>

<body style="background-image: url(img/fondoPerfil.jpg);
background-size: cover">
    <div class="container mt-5 mb-5">
        <div class="card f3 b">
            <div class="card-header bg-dark text-white text-center" style="border-top-left-radius: 8px; border-top-right-radius: 8px;">
                <h4>Proceso cargo <?php echo $_GET["id"] ?></h4>
                <ul class="nav nav-tabs card-header-tabs" id="bologna-list" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#info" role="tab" aria-controls="description" aria-selected="true">Informacion basica</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#aspirantes" role="tab" aria-controls="history" aria-selected="false">Aspirantes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#deals" role="tab" aria-controls="deals" aria-selected="false">Estadisticas</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">

                <div class="tab-content mt-3">
                    <div class="tab-pane active" id="info" role="tabpanel">
                        <h4 class="card-title"><?php echo "Cargo " . $cargo->getNombre() ?></h4>
                        <h6 class="card-subtitle mb-2"><?php echo "Solicitado por " . $jefe->getNombre() . " " . $jefe->getApellido() ?></h6>
                        <p class="card-text"><?php echo $cargo->getDescripcion() ?></p>
                        <p class="card-text"><b>Fecha limite : </b><?php echo $cargo->getFecha() ?></p>
                        <p class="card-text"><b>Vacantes disponibles: </b><?php echo $cargo->getVacantes() ?></p>
                        <p class="card-text"><b>Aspirantes a la fecha: </b><?php echo count($aspirantes) ?></p>

                    </div>

                    <div class="tab-pane" id="aspirantes" role="tabpanel" aria-labelledby="history-tab">

                    </div>

                    <div class="tab-pane" id="deals" role="tabpanel" aria-labelledby="deals-tab">
                        <?php
                        include "presentacion/encuesta/grafica.php";
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/recursos/listaAsp.php") . "&id=" . $cargo->geIdCargo() ?>";
            $("#aspirantes").load(url);
        })
        $('#bologna-list a').on('click', function(e) {
            e.preventDefault()
            $(this).tab('show')
        })
    </script>