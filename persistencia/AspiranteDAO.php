<?php
class AspiranteDAO{
    private $idAspirante;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $edad;
    private $sexo;
    private $civil;
    private $descripcion;
    private $foto;
    private $estado;

    public function AspiranteDAO($idAspirante = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $edad = "", $sexo = "", $civil = "", $descripcion = "", $estado = ""){
        $this -> idAspirante = $idAspirante;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> edad = $edad;
        $this -> sexo = $sexo;
        $this -> civil = $civil;
        $this -> descripcion = $descripcion;
        $this -> estado = $estado;
    }    
    public function autenticar(){
        return "select idAspirante, estado
                from aspirante
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto, idAspirante, Edad, Sexo, Civil, Descripcion, estado
                from aspirante
                where idAspirante = '" . $this -> idAspirante .  "'";
    }
    
    public function editar($param,$edicion){
        return "update Aspirante
                set ".$param." = '" .$edicion. 
                "' where idAspirante = '" . $this -> idAspirante .  "'";
    }

    public function insertarFactBasic($cargo,$edad,$exp,$sexo,$civil,$descripcion){
        return "insert into aspirante (Edad, Experiencia,Sexo,Civil,IdAspirante,Descripcion) 
        values ('".$edad."','".$exp."','".$sexo."','".$civil."','".$descripcion."','".$this-> idAspirante."')";   
    }

    public function insertar(){
        return "insert into Aspirante (nombre, cantidad, precio, descripcion, foto)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> precio . "', '" .$this -> descripcion . "', '" . $this -> foto ."')";
    }
    public function editarA(){
        return "update aspirante
                set edad = '" . $this -> edad . "', sexo = '" . $this -> sexo . "', civil = '" . $this -> civil . "', descripcion = '" . $this -> descripcion . "'"  ."
                where idAspirante = '" . $this -> idAspirante .  "'";
    }
    public function editarFoto(){
        return "update aspirante
                set foto = '" . $this -> foto  . "'
                where idAspirante = '" . $this -> idAspirante .  "'";
    }
    public function consultarPaginacion($cantidad, $pagina){
        return "select idAspirante, nombre, apellido, correo, foto, estado
                from aspirante
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarPaginacionRec($cantidad, $pagina,$idCargo){
        return "select idAspirante, Nombre, Apellido, Correo, Foto, Edad, Sexo, Civil, Descripcion
                from aspirantesrecursos 
                where idCargo ='".$idCargo."'
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idAspirante)
                from aspirante";
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro)
    {
        return "select idAspirante, nombre, apellido, correo, foto, estado
        from aspirante
        where (nombre like '%" . $filtro . "%' or apellido like '" . $filtro . "%' or correo like '" . $filtro . "%') 
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function existeCorreo(){
        return "select correo
                from aspirante
                where correo = '" . $this -> correo .  "'";
    }
    public function registrar(){
        return "insert into aspirante (nombre, apellido, correo, clave, estado)
                values ('" . $this -> nombre . "', '" . $this -> apellido . "', '" . $this -> correo . "', '" . md5($this -> clave) . "', '" . $this -> estado . "')";
    }

}

?>