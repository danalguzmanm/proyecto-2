<?php
class LogDAO
{
    private $idLog;
    private $fecha;
    private $hora;
    private $datos;
    private $idLogAccion;
    private $nombre;
    private $actor;
    
    public function LogDAO($idLog = "", $fecha = "", $hora = "", $datos = "", $idLogAccion = "", $nombre = "", $actor)
    {
        $this-> idLog = $idLog;
        $this-> fecha = $fecha;
        $this-> hora = $hora;
        $this-> datos = $datos;
        $this-> idLogAccion = $idLogAccion;
        $this-> nombre = $nombre;
        $this -> actor = $actor;
    }

    public function insertar(){
        return "Insert into log (idLog,Fecha,Hora,Datos,idLogAccion)
        values ('".$this -> idLog."',NOW(),NOW(),'".$this-> datos."','".$this -> idLogAccion."')";       
    }

    public function asociarA($id){
        return "Insert into aspirantelog (idAspirante,idLog)
        values ('".$id."','".$this -> idLog."')";        
    }

    public function asociarJ($id){
        return "Insert into jefelog (idJefe,idLog)
        values ('".$id."','".$this -> idLog."')";        
    }

    public function asociarR($id){
        return "Insert into recursoslog (idRecursos,idLog)
        values ('".$id."','".$this -> idLog."')";        
    }

    public function asociarE($id){
        return "Insert into entrevistadorlog (idEntrevistador,idLog)
        values ('".$id."','".$this -> idLog."')";        
    }

    public function asociarAd($id){
        return "Insert into administradorlog (idAdministrador,idLog)
        values ('".$id."','".$this -> idLog."')";        
    }

    public function consultar()
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idAspirante 
        from verlogasp
        where idLog= '".$this-> idLog."'";
    }

    public function consultarPaginacionAspF($cantidad, $pagina, $filtro)
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idAspirante 
        from verlogasp
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') 
        ORDER BY Fecha DESC, Hora DESC
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadAspF($filtro)
    {
        return "select count(idLog) FROM verlogasp
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') ";
    }

    public function consultarJ()
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idJefe 
        from verlogJefe
        where idLog= '".$this-> idLog."'";
    }

    public function consultarPaginacionJefF($cantidad, $pagina, $filtro)
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idJefe 
        from verlogJefe
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') 
        ORDER BY Fecha DESC, Hora DESC
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadJefF($filtro)
    {
        return "select count(idLog) FROM verlogJefe
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') ";
    }

    public function consultarR()
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idRecursos
        from verlogrec
        where idLog= '".$this-> idLog."'";
    }

    public function consultarPaginacionRecF($cantidad, $pagina, $filtro)
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idRecursos
        from verlogrec
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') 
        ORDER BY Fecha DESC, Hora DESC
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadRecF($filtro)
    {
        return "select count(idLog) FROM verlogrec
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') ";
    }

    public function consultarE()
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idEntrevistador
        from verloge
        where idLog= '".$this-> idLog."'";
    }

    public function consultarPaginacionEF($cantidad, $pagina, $filtro)
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idEntrevistador
        from verloge
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') 
        ORDER BY Fecha DESC, Hora DESC
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadEF($filtro)
    {
        return "select count(idLog) FROM verloge
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') ";
    }

    public function consultarAd()
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idAdministrador
        from verlogad 
        where idLog= '".$this-> idLog."'";
    }

    public function consultarPaginacionAdF($cantidad, $pagina, $filtro)
    {
        return "select idLog, Fecha, Hora, Datos, idLogAccion, Accion,idAdministrador
        from verlogad 
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') 
        ORDER BY Fecha DESC, Hora DESC
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadAdF($filtro)
    {
        return "select count(idLog) FROM verlogad 
        where (Actor like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or Accion like '" . $filtro . "%') ";
    }
}
