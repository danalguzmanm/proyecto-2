<?php
class ContratacionDAO{
    private $idContratacion;
    private $idRecursos;
    private $idCargo;

    public function ContratacionDAO($idContratacion = "", $idRecursos = "", $idCargo = ""){
        $this -> idContratacion = $idContratacion;
        $this -> idRecursos = $idRecursos;
        $this -> idCargo = $idCargo;
    }
    
    public function insertar(){
        return "Insert into contratacion (idContratacion,idRecursos,idCargo) values
        ('".$this -> idContratacion."','".$this -> idRecursos."','".$this -> idCargo."')";     
    }

    public function consultarPaginacionRec($cantidad, $pagina)
    {
        return "select idContratacion, idRecursos, idCargo from contratacion 
                where idRecursos='" . $this-> idRecursos . "' 
                limit " . (($pagina - 1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarJefe(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCant(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacion($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacionJefe($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionJefe($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultar(){
        return "select idContratacion,idRecursos,idCargo from contratacion
        where idCargo = '".$this -> idCargo."'";
    }

    public function consultarRec(){
        return "select idContratacion,idRecursos,idCargo from contratacion
        where idRec = '".$this -> idRecursos."'";
    }

    public function consultarRec2(){
        return "select idContratacion,idRecursos,idCargo from contratacion
        where idRecursos = '".$this -> idRecursos."'";
    }

     // para uso de ajax tabla
     public function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarTodos());
        $administrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Administrador($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($administrador, $c);
        }
        $this -> conexion -> cerrar();
        return $administrador;
    }

    public function consultarCantidadFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltro($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltro($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
    
    public function consultarCantidadFiltroJ($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltroJ($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultarPaginacionFiltroJ($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltroJ($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
}
