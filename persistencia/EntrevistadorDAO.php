<?php
class EntrevistadorDAO{
    private $idEntrevistador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $conexion;

    public function EntrevistadorDAO($idEntrevistador = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "",$estado){
        $this -> idEntrevistador = $idEntrevistador;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
    }

    public function BuscarLog(){
        return "select idAdministrador
                from Administrador 
                where correo = '" . $this -> correo .  "'";
    }

    public function autenticar(){
        return "select idEntrevistador, estado
                from Entrevistador 
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto, clave
                from Entrevistador
                where idEntrevistador = '" . $this -> idEntrevistador .  "'";
    }

    public function consultarTodos(){
        return "select idAdministrador, nombre, apellido, correo
                from Administrador";
    }

    public function consultarPaginacion($cantidad, $pagina)
    {
        return "select idEntrevistador, nombre, apellido, correo, foto, estado
        from entrevistador
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }


    public function consultarCantidad(){
        return "select count(idAdministrador)
                from Administrador";
    }
    
    public function consultarCantidadFiltro($filtro){
        return "select count(idAdministrador)
                from Administrador
                where idAdministrador like '%" . $filtro . "%' or nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%' or correo like '" . $filtro . "%'   ";             
    }
    
    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro)
    {
        return "select idEntrevistador, nombre, apellido, correo, foto, estado
        from entrevistador
        where (nombre like '%" . $filtro . "%' or apellido like '" . $filtro . "%' or correo like '" . $filtro . "%') 
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }


    public function editarColumn($column,$editableObj,$id){
        $sql = "UPDATE Administrador set " . $column . " = '".$editableObj."' WHERE  idAdministrador=".$id;
        return $sql;
    }
 

    public function existeCorreo(){
        return "select correo
                from administrador
                where correo = '" . $this -> correo .  "'";
    }
    
    public function editar($param,$edicion){
        return "update Entrevistador
                set ".$param." = '" .$edicion. 
                "' where idEntrevistador = '" . $this -> idEntrevistador .  "'";
    }

    public function editarClave(){
        return "update administrador
                set clave = '" .  md5($this -> clave)  . 
                "' where idAdministrador = '" . $this -> idAdministrador .  "'";
    }

    public function consultarTodosId(){
        return "select idEntrevistador from entrevistador";
    }

    public function editarFoto(){
        return "update entrevistador
                set foto = '" . $this -> foto  . "'
                where idEntrevistador = '" . $this -> idEntrevistador .  "'";
    }
}

?>