<?php
class JefeDAO{
    private $idJefe;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function JefeDAO($idJefe = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado= ""){
        $this -> idJefe = $idJefe;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
    }

    public function autenticar(){
        return "select idJefe, estado
                from Jefe 
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto, clave
                from Jefe
                where idJefe = '" . $this -> idJefe .  "'";
    }

    public function consultarTodos(){
        return "select idAdministrador, nombre, apellido, correo
                from Administrador";
    } 
 
    public function consultarPaginacion($cantidad, $pagina)
    {
        return "select idJefe, nombre, apellido, correo, foto, estado
        from jefe
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idAdministrador)
                from Administrador";
    }

    public function editar($param,$edicion){
        return "update Jefe
                set ".$param." = '" .$edicion. 
                "' where idJefe = '" . $this -> idJefe .  "'";
    }
    
    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro)
    {
        return "select idJefe, nombre, apellido, correo, foto, estado
        from jefe
        where (nombre like '%" . $filtro . "%' or apellido like '" . $filtro . "%' or correo like '" . $filtro . "%') 
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function editarFoto(){
        return "update jefe
                set foto = '" . $this -> foto  . "'
                where idJefe = '" . $this -> idJefe .  "'";
    }

}

?>