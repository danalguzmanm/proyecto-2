<?php
class CargoDAO
{
    private $idCargo;
    private $nombre;
    private $vacantes;
    private $fecha;
    private $idJefe;
    private $estado;
    private $descripcion;


    public function geIdCargo()
    {
        return $this->idCargo;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getVacantes()
    {
        return $this->vacantes;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getIdJefe()
    {
        return $this->idJefe;
    }

    public function getEstadp()
    {
        return $this->estado;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function CargoDAO($idCargo = "", $nombre = "", $vacantes = "", $fecha = "", $idJefe = "", $estado = "",$descripcion ="")
    {
        $this->idCargo = $idCargo;
        $this->nombre = $nombre;
        $this->vacantes = $vacantes;
        $this->fecha = $fecha;
        $this->idJefe = $idJefe;
        $this->estado = $estado;
        $this->descripcion = $descripcion;
    }

    public function insertar()
    {
        return "insert into Cargo (idCargo,nombre, vacantes, fecha, estado, idJefe, descripcion)
        values ('".$this -> idCargo."','" . $this->nombre . "', '" . $this->vacantes . "', '" . $this->fecha . "', 'Activo','" . $this->idJefe . "','" . $this->descripcion . "')";
    }

    public function consultarJefe()
    {
        return "select nombre, vacantes, fecha, estado from Cargo 
        where idJefe='" . $this->idJefe . "'";
    }

    public function consultarCant()
    {
        return "select nombre, vacantes, fecha, estado from Cargo ";
    }

    public function consultar()
    {
        return "select idCargo, nombre, vacantes, fecha, estado, descripcion,idJefe from Cargo 
                where idCargo='" . $this->idCargo . "'";
    }

    // para uso de ajax tabla
    public function consultarTodos()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->administradorDAO->consultarTodos());
        $administrador = array();
        while (($resultado = $this->conexion->extraer()) != null) {
            $c = new Administrador($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($administrador, $c);
        }
        $this->conexion->cerrar();
        return $administrador;
    }

    public function consultarPaginacionJefe($cantidad, $pagina)
    {
        return "select idCargo, nombre, vacantes, fecha, estado, descripcion from Cargo 
                where idJefe='" . $this->idJefe . "' 
                limit " . (($pagina - 1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarPaginacionRec($cantidad, $pagina)
    {
        return "select idCargo, nombre, vacantes, fecha, estado, descripcion from cargo 
                where id='" . $this->idJefe . "'and estado= 'Activo' 
                limit " . (($pagina - 1) * $cantidad) . ", " . $cantidad ;
    }

    public function consultarPaginacion($cantidad, $pagina)
    {
        return "select idCargo, nombre, vacantes, fecha, estado, descripcion from Cargo  
                limit " . (($pagina - 1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadFiltro($filtro)
    {
        return "select count(idCargo) from Cargo 
        where nombre like '%" . $filtro . "%' or vacantes like '" . $filtro . "%' or fecha like '" . $filtro . "%'";
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro)
    {
        return "select idCargo, nombre, vacantes, fecha, estado, descripcion from Cargo 
        where nombre like '%" . $filtro . "%' or vacantes like '" . $filtro . "%' or fecha like '" . $filtro . "%'
        limit " . (($pagina - 1) * $cantidad) . ", " . $cantidad ;
    }

    public function consultarCantidadFiltroJ($filtro)
    {
        return "select count(idCargo) from Cargo 
        where nombre like '%" . $filtro . "%' or vacantes like '" . $filtro . "%' or fecha like '" . $filtro . "%'
        AND idJefe ='".$this -> idJefe."'";
    }

    public function consultarCantidadFiltroRec($filtro)
    {
        return "select count(idCargo) from Cargo 
        where nombre like '%" . $filtro . "%' or vacantes like '" . $filtro . "%' or fecha like '" . $filtro . "%'
        AND idCargo ='".$this -> idCargo."'";
    }

    public function consultarPaginacionFiltroJ($cantidad, $pagina, $filtro)
    {
        return "select idCargo, nombre, vacantes, fecha, estado, descripcion from Cargo 
        where (nombre like '%" . $filtro . "%' or vacantes like '" . $filtro . "%' or fecha like '" . $filtro . "%') 
        AND idJefe ='".$this -> idJefe."'";
    }



    public function consultarPaginacionFiltroRec($cantidad, $pagina, $filtro,$idRec)
    {
        return "select idCargo,Nombre,Vacantes,Fecha,Estado,Descripcion,idJefe from paginacionrecursos 
        where (Nombre like '%" . $filtro . "%' or Vacantes like '" . $filtro . "%' or Fecha like '" . $filtro . "%') 
        AND idRecursos ='".$idRec."'and estado='Activo'";

    }
    public function editar($param,$edicion){
        return "update cargo
                set ".$param." = '" .$edicion. 
                "' where idCargo = '" . $this -> idCargo .  "'";
    }
}
