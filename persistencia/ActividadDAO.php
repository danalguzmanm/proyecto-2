<?php
class ActividadDAO{
    private $idActividad;
    private $fecha;
    private $descripcion;
    private $idActor;
    private $idContratacion;
    private $actividadDAO;
    private $tipoActor;

    public function ActividadDAO($idActividad = "", $fecha = "", $descripcion = "", $idContratacion = "", $idActor = "", $tipoActor = ""){
        $this -> idActividad = $idActividad;
        $this -> fecha = $fecha;
        $this -> descripcion = $descripcion;
        $this -> idContratacion = $idContratacion;
        $this -> idActor = $idActor;
        $this -> tipoActor = $tipoActor;
    }
    
    public function insertar(){
        return "Insert into actividad (Fecha,Descripcion,idContratacion,idActor, tipoActor)
        values (NOW(), '".$this -> descripcion."','".$this -> idContratacion."','".$this -> idActor."','".$this-> tipoActor."')";       
    }

    public function consultarJefe(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarCant(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarJefe());
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo("",$resultado[0], $resultado[1], $resultado[2],"" ,$resultado[3]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacion($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarPaginacionJefe($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionJefe($cantidad, $pagina));
        $cargos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargos, $c);
        }
        $this -> conexion -> cerrar();
        return $cargos;
    }

    public function consultarTodos(){
        return "Select idActividad, Fecha, Descripcion, idContratacion, idActor, TipoActor 
        from actividad
        where idContratacion ='".$this -> idContratacion."' ORDER BY Fecha ASC";
    }


    public function consultarCantidadFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltro($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltro($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
    
    public function consultarCantidadFiltroJ($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarCantidadFiltroJ($filtro));
        $this -> conexion -> cerrar();        
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont =0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }        
        return $cont;
    }

    public function consultarPaginacionFiltroJ($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cargoDAO -> consultarPaginacionFiltroJ($cantidad, $pagina, $filtro));
        $cargo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cargo($resultado[0],$resultado[1], $resultado[2], $resultado[3],"" ,$resultado[4],$resultado[5]);
            array_push($cargo, $c);
        }
        $this -> conexion -> cerrar();
        return $cargo;
    }
}
