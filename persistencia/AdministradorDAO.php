<?php
class AdministradorDAO{
    private $idAdministrador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function AdministradorDAO($idAdministrador = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idAdministrador = $idAdministrador;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
    }

    public function BuscarLog(){
        return "select idAdministrador
                from Administrador 
                where correo = '" . $this -> correo .  "'";
    }

    public function autenticar(){
        return "select idAdministrador , estado
                from Administrador 
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto
                from Administrador
                where idAdministrador = '" . $this -> idAdministrador .  "'";
    }

    public function consultarTodos(){
        return "select idAdministrador, nombre, apellido, correo
                from Administrador";
    }

    public function consultarPaginacion($cantidad, $pagina){
        return "select idAdministrador, nombre, apellido, correo, foto, estado
                from administrador
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro)
    {
        return "select idAdministrador, nombre, apellido, correo, foto, estado
        from administrador
        where (nombre like '%" . $filtro . "%' or apellido like '" . $filtro . "%' or correo like '" . $filtro . "%') 
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idAdministrador)
                from Administrador";
    }
    
    public function consultarCantidadFiltro($filtro){
        return "select count(idAdministrador)
                from Administrador
                where idAdministrador like '%" . $filtro . "%' or nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%' or correo like '" . $filtro . "%'   ";             
    }

    public function editarColumn($column,$editableObj,$id){
        $sql = "UPDATE Administrador set " . $column . " = '".$editableObj."' WHERE  idAdministrador=".$id;
        return $sql;
    }
 

    public function existeCorreo(){
        return "select correo
                from administrador
                where correo = '" . $this -> correo .  "'";
    }
    
    public function editar($param,$edicion){
        return "update administrador
                set ".$param." = '" .$edicion. 
                "' where idAdministrador = '" . $this -> idAdministrador .  "'";
    }

    public function editarClave(){
        return "update administrador
                set clave = '" .  md5($this -> clave)  . 
                "' where idAdministrador = '" . $this -> idAdministrador .  "'";
    }

    public function editarFoto(){
        return "update administrador
                set foto = '" . $this -> foto  . "'
                where idAdministrador = '" . $this -> idAdministrador .  "'";
    }
}

?>