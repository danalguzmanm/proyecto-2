<?php
class RecursosDAO{
    private $idRecursos;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function RecursosDAO($idRecursos = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado= ""){
        $this -> idRecursos = $idRecursos;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;  
        $this -> estado = $estado;      
    }    
    public function autenticar(){
        return "select IdRecursos, estado
                from recursos
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto
                from recursos
                where IdRecursos = '" . $this -> idRecursos .  "'";
    }
    
    public function editar($param,$edicion){
        return "update recursos
                set ".$param." = '" .$edicion. 
                "' where IdRecursos = '" . $this -> idRecursos .  "'";
    }

    public function consultarPaginacion($cantidad, $pagina)
    {
        return "select idRecursos, nombre, apellido, correo, foto, estado
        from recursos
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idAspirante)
                from aspirante";
    }

    public function consultarTodosId(){
        return "select idRecursos from recursos";
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro)
    {
        return "select idRecursos, nombre, apellido, correo, foto, estado
        from recursos
        where (nombre like '%" . $filtro . "%' or apellido like '" . $filtro . "%' or correo like '" . $filtro . "%') 
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function editarFoto(){
        return "update recursos
                set foto = '" . $this -> foto  . "'
                where idRecursos = '" . $this -> idRecursos .  "'";
    }

    /* public function insertarFactBasic($cargo,$edad,$exp,$sexo,$civil){
        return "insert into adaptacion (Cargo,Edad, Experiencia,Sexo, EstadoCivil,IdAspirante) 
        values ('".$cargo."','".$edad."','".$exp."','".$sexo."','".$civil."','".$this-> idAspirante."')";   
    } */
    /* public function insertar(){
        return "insert into Aspirante (nombre, cantidad, precio, descripcion, foto)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> precio . "', '" .$this -> descripcion . "', '" . $this -> foto ."')";
    } */
}
