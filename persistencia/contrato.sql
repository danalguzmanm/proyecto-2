-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-08-2020 a las 06:28:17
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.3.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `contrato`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `idActividad` int(11) NOT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Descripcion` varchar(400) DEFAULT NULL,
  `idContratacion` int(11) NOT NULL,
  `idActor` int(11) DEFAULT NULL,
  `TipoActor` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`idActividad`, `Fecha`, `Descripcion`, `idContratacion`, `idActor`, `TipoActor`) VALUES
(3, '2020-08-19 17:31:56', 'Inicio del proceso', 2, 1, 'Jefe'),
(5, '2020-08-19 17:33:33', 'se postulo al cargo', 2, 1, 'Aspirante'),
(7, '2020-08-20 19:48:08', 'se postuló al cargo', 2, 2, 'Aspirante'),
(9, '2020-08-21 22:01:40', 'Inicio del proceso', 4, 1, 'Jefe'),
(10, '2020-08-21 23:32:12', 'evaluó al aspirante Juanito Reyes otorgandole un puntaje de 27', 2, 1, 'Entrevistador'),
(11, '2020-08-24 20:47:16', 'Inicio del proceso', 5, 1, 'Jefe'),
(12, '2020-08-24 20:48:03', 'se postuló al cargo', 5, 2, 'Aspirante'),
(13, '2020-08-25 20:15:58', 'Inicio del proceso', 6, 1, 'Jefe'),
(14, '2020-08-25 20:17:54', 'se postuló al cargo', 6, 2, 'Aspirante'),
(15, '2020-08-25 20:20:23', 'evaluó al aspirante Juanito Reyes otorgandole un puntaje de 27', 2, 1, 'Entrevistador'),
(16, '2020-08-25 20:36:30', 'Inicio del proceso', 7, 1, 'Jefe'),
(17, '2020-08-25 20:40:46', 'se postuló al cargo', 7, 1, 'Aspirante'),
(18, '2020-08-25 20:42:43', 'se postuló al cargo', 7, 2, 'Aspirante'),
(19, '2020-08-25 22:06:23', 'Inicio del proceso', 8, 1, 'Jefe'),
(20, '2020-08-25 22:36:29', 'Inicio del proceso', 9, 1, 'Jefe'),
(21, '2020-08-25 22:54:44', 'se postuló al cargo', 9, 1, 'Aspirante'),
(22, '2020-08-25 22:58:30', 'contratacion del Aspirante', 9, 1, 'recursos'),
(23, '2020-08-25 22:58:30', 'finalizacion del proceso', 9, 1, 'recursos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Correo` varchar(45) DEFAULT NULL,
  `Clave` varchar(45) DEFAULT NULL,
  `Foto` varchar(45) DEFAULT NULL,
  `Estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `Nombre`, `Apellido`, `Correo`, `Clave`, `Foto`, `Estado`) VALUES
(1, 'Laura', 'Garcia', 'admin@correo.com', '202cb962ac59075b964b07152d234b70', 'img/siempre/img_1597958734.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradorlog`
--

CREATE TABLE `administradorlog` (
  `idAdministrador` int(11) NOT NULL,
  `idLog` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administradorlog`
--

INSERT INTO `administradorlog` (`idAdministrador`, `idLog`) VALUES
(1, 1598123594),
(1, 1598126393),
(1, 1598300327),
(1, 1598314882),
(1, 1598315185),
(1, 1598315275),
(1, 1598414461),
(1, 1598415748),
(1, 1598415913),
(1, 1598415933);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aspirante`
--

CREATE TABLE `aspirante` (
  `idAspirante` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Correo` varchar(45) DEFAULT NULL,
  `Clave` varchar(45) DEFAULT NULL,
  `Foto` varchar(45) DEFAULT NULL,
  `Edad` int(11) DEFAULT NULL,
  `Sexo` varchar(45) DEFAULT NULL,
  `Civil` varchar(45) DEFAULT NULL,
  `Descripcion` varchar(400) DEFAULT NULL,
  `Estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `aspirante`
--

INSERT INTO `aspirante` (`idAspirante`, `Nombre`, `Apellido`, `Correo`, `Clave`, `Foto`, `Edad`, `Sexo`, `Civil`, `Descripcion`, `Estado`) VALUES
(1, 'Juanito', 'Reyes', 'Franco@correo.com', '202cb962ac59075b964b07152d234b70', 'img/siempre/img_1597950548.jpg', 32, 'Hombre', 'Soltero', '2131wsdasd12eqw', 1),
(2, 'Juanit', 'Reyes', 'Juan@correo.com', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL, NULL, NULL, 1),
(3, 'Pepe', 'Garcia', 'nose@correo.com', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL, NULL, NULL, 2),
(5, 'Juan', 'Petunio', 'petunio@correo.com', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL, NULL, NULL, 0),
(6, 'sad', 'sad', 'sad@correo.com', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL, NULL, NULL, 0),
(7, 'Paula', 'wer', '4@4.com', 'a87ff679a2f3e71d9181a67b7542122c', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aspirantelog`
--

CREATE TABLE `aspirantelog` (
  `idAspirante` int(11) NOT NULL,
  `idLog` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `aspirantelog`
--

INSERT INTO `aspirantelog` (`idAspirante`, `idLog`) VALUES
(1, 1597986658),
(1, 1597986667),
(1, 1597986668),
(1, 1597986685),
(1, 1598029683),
(1, 1598029736),
(1, 1598029845),
(1, 1598068744),
(1, 1598405831),
(1, 1598405864),
(1, 1598405887),
(1, 1598405902),
(1, 1598406034),
(1, 1598406046),
(1, 1598411218),
(1, 1598411273),
(1, 1598413403),
(1, 1598414084),
(2, 1598320075),
(2, 1598320083),
(2, 1598404624),
(2, 1598404674),
(2, 1598406157),
(2, 1598406162);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `aspirantesrecursos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `aspirantesrecursos` (
`idAspirante` int(11)
,`Nombre` varchar(45)
,`Apellido` varchar(45)
,`Correo` varchar(45)
,`Foto` varchar(45)
,`Edad` int(11)
,`Sexo` varchar(45)
,`Civil` varchar(45)
,`Descripcion` varchar(400)
,`idCargo` int(11)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `idCargo` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Vacantes` varchar(45) DEFAULT NULL,
  `Descripcion` varchar(400) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `idJefe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`idCargo`, `Nombre`, `Vacantes`, `Descripcion`, `Fecha`, `Estado`, `idJefe`) VALUES
(1597876302, 'Desarrollador', '3', 'Importante empresa del sector de logística y transporte requiere para su equipo de trabajo desarrollador de sistemas profesional graduado en ingeniería de sistemas con especialización en desarrollo de software o similares, con conocimiento en desarrollo y programación de software con experiencia en elaborar sistemas informáticos, experiencia mínima de 3 años en el cargo de desarrollador programado', '2020-08-29', 'Activo', 1),
(1598065300, 'Analista', '2', 'se requiere ANALISTA DE SG-SST, NIVEL EDUCATIVO MÍNIMO REQUERIDO TECNICO O TECNOLOGO en SG-SST horario de trabajo de lunes a viernes de 8:00 am a 5:00 pm y sábados 8:00 am 2:00 pm CONOCIMIENTOS ESPECÍFICOS :Experiencia en SG-SST Implementacion de sistemas de gestiona en seguridad y salud en el trabajo.', '2020-09-03', 'Activo', 1),
(1598320035, 'sadas', '1', 'asdasdasdsa', '2020-08-13', 'Activo', 1),
(1598404558, 'Secretaria', '1', 'asdasdasdsasdasd', '2020-09-05', 'Activo', 1),
(1598405789, 'nose', '1', 'qweqwdsdwqewdwa', '2020-09-05', 'Activo', 1),
(1598411182, 'Desarrollador', '1', 'qweqw qweqwe qweqweqw', '2020-09-05', 'Activo', 1),
(1598412989, 'Pelota', '0', 'aqdwa wdadwdasds', '2020-09-05', 'finalizado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargoaspirante`
--

CREATE TABLE `cargoaspirante` (
  `idCargoAspirante` int(11) NOT NULL,
  `idCargo` int(11) NOT NULL,
  `idAspirante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cargoaspirante`
--

INSERT INTO `cargoaspirante` (`idCargoAspirante`, `idCargo`, `idAspirante`) VALUES
(4, 1597876302, 1),
(7, 1597876302, 2),
(8, 1598320035, 2),
(9, 1598404558, 2),
(10, 1598405789, 1),
(12, 1598412989, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratacion`
--

CREATE TABLE `contratacion` (
  `idContratacion` int(11) NOT NULL,
  `idRecursos` int(11) NOT NULL,
  `idCargo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `contratacion`
--

INSERT INTO `contratacion` (`idContratacion`, `idRecursos`, `idCargo`) VALUES
(2, 1, 1597876302),
(4, 1, 1598065300),
(5, 1, 1598320035),
(6, 1, 1598404558),
(7, 1, 1598405789),
(8, 1, 1598411182),
(9, 1, 1598412989);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuesta`
--

CREATE TABLE `encuesta` (
  `idEncuesta` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Puntaje` int(11) DEFAULT NULL,
  `idAspirante` int(11) NOT NULL,
  `idEntrevistador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `encuesta`
--

INSERT INTO `encuesta` (`idEncuesta`, `Nombre`, `Puntaje`, `idAspirante`, `idEntrevistador`) VALUES
(1597970772, 'Razonamiento', 1, 2, 1),
(1597970790, 'Personalidad', 16, 2, 1),
(1597970832, 'Espacial', 2, 2, 1),
(1598404823, 'entrevista', 27, 1, 1),
(1598405864, 'Espacial', 3, 1, 1),
(1598405887, 'Personalidad', 18, 1, 1),
(1598405902, 'Razonamiento', 2, 1, 1),
(1598414151, 'entrevista', 25, 7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrevistador`
--

CREATE TABLE `entrevistador` (
  `idEntrevistador` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Correo` varchar(45) DEFAULT NULL,
  `Clave` varchar(45) DEFAULT NULL,
  `Foto` varchar(45) DEFAULT NULL,
  `Estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `entrevistador`
--

INSERT INTO `entrevistador` (`idEntrevistador`, `Nombre`, `Apellido`, `Correo`, `Clave`, `Foto`, `Estado`) VALUES
(1, 'Juanita', 'Alcachofa', '1@1.com', 'c4ca4238a0b923820dcc509a6f75849b', 'img/siempre/img_1598404777.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrevistadorlog`
--

CREATE TABLE `entrevistadorlog` (
  `idEntrevistador` int(11) NOT NULL,
  `idLog` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `entrevistadorlog`
--

INSERT INTO `entrevistadorlog` (`idEntrevistador`, `idLog`) VALUES
(1, 1598067984),
(1, 1598068812),
(1, 1598068876),
(1, 1598069265),
(1, 1598069485),
(1, 1598070708),
(1, 1598315632),
(1, 1598404750),
(1, 1598404781),
(1, 1598404787),
(1, 1598404823),
(1, 1598414125),
(1, 1598414151);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jefe`
--

CREATE TABLE `jefe` (
  `idJefe` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Correo` varchar(45) DEFAULT NULL,
  `Clave` varchar(45) DEFAULT NULL,
  `Foto` varchar(45) DEFAULT NULL,
  `Estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `jefe`
--

INSERT INTO `jefe` (`idJefe`, `Nombre`, `Apellido`, `Correo`, `Clave`, `Foto`, `Estado`) VALUES
(1, 'Pauladasa', 'Guzman', '123@123.com', '202cb962ac59075b964b07152d234b70', 'img/siempre/img_1598413373.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jefelog`
--

CREATE TABLE `jefelog` (
  `idJefe` int(11) NOT NULL,
  `idLog` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `jefelog`
--

INSERT INTO `jefelog` (`idJefe`, `idLog`) VALUES
(1, 1598063864),
(1, 1598065300),
(1, 1598066695),
(1, 1598070267),
(1, 1598315081),
(1, 1598315357),
(1, 1598320022),
(1, 1598320035),
(1, 1598404508),
(1, 1598404558),
(1, 1598405777),
(1, 1598405789),
(1, 1598411150),
(1, 1598411182),
(1, 1598411201),
(1, 1598411206),
(1, 1598411247),
(1, 1598412714),
(1, 1598412989),
(1, 1598413329),
(1, 1598413334),
(1, 1598413340),
(1, 1598413343),
(1, 1598413346),
(1, 1598413377),
(1, 1598413379);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `idLog` int(11) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Hora` time DEFAULT NULL,
  `Datos` varchar(400) DEFAULT NULL,
  `idLogAccion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`idLog`, `Fecha`, `Hora`, `Datos`, `idLogAccion`) VALUES
(1597986658, '2020-08-21', '00:10:58', '', 1),
(1597986667, '2020-08-21', '00:11:07', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: /Apellido: Reye/Edad:32/Correo: Franco@correo.com/Sexo: Masculino/Civil: Soltero/Descripción: Como recién graduado, me encantaría poder entrar a formar parte de una empresa en la que poder aplicar todos mis conocimientos y, al mismo tiempo, que me permita desarrollarme profesionalmente.', 3),
(1597986668, '2020-08-21', '00:11:08', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Fr/Apellido: Reye/Edad:32/Correo: Franco@correo.com/Sexo: Masculino/Civil: Soltero/Descripción: Como recién graduado, me encantaría poder entrar a formar parte de una empresa en la que poder aplicar todos mis conocimientos y, al mismo tiempo, que me permita desarrollarme profesionalmente.', 3),
(1597986685, '2020-08-21', '00:11:25', 'Resolvio encuesta de Personalidad y obtuvo un puntaje de 20', 2),
(1598029683, '2020-08-21', '12:08:04', '', 1),
(1598029736, '2020-08-21', '12:08:56', 'Editó el apartado apellido. Informacion antes de la edición=Nombre: Franco/Apellido: Reye/Edad:32/Correo: Franco@correo.com/Sexo: Masculino/Civil: Soltero/Descripción: Como recién graduado, me encantaría poder entrar a formar parte de una empresa en la que poder aplicar todos mis conocimientos y, al mismo tiempo, que me permita desarrollarme profesionalmente.', 3),
(1598029845, '2020-08-21', '12:10:45', '', 1),
(1598063864, '2020-08-21', '21:37:44', '', 1),
(1598065300, '2020-08-21', '22:01:40', 'Creacion de proceso cargo: 1598065300', 5),
(1598066695, '2020-08-21', '22:24:55', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Paulo/Apellido: Guzman/Correo: 123@123.com', 3),
(1598067047, '2020-08-21', '22:30:47', '', 1),
(1598067984, '2020-08-21', '22:46:24', '', 1),
(1598068744, '2020-08-21', '22:59:04', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Juanita/Apellido: Reyes/Edad:32/Correo: Franco@correo.com/Sexo: Masculino/Civil: Soltero/Descripción: Como recién graduado, me encantaría poder entrar a formar parte de una empresa en la que poder aplicar todos mis conocimientos y, al mismo tiempo, que me permita desarrollarme profesionalmente.', 3),
(1598068812, '2020-08-21', '23:00:12', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Juanita/Apellido: Alcachofa/Correo: 1@1.com', 3),
(1598068876, '2020-08-21', '23:01:16', '', 1),
(1598068885, '2020-08-21', '23:01:26', '', 1),
(1598069265, '2020-08-21', '23:07:45', '', 1),
(1598069485, '2020-08-21', '23:11:25', '1:26', 6),
(1598070267, '2020-08-21', '23:24:27', '', 1),
(1598070708, '2020-08-21', '23:31:48', '', 1),
(1598070879, '2020-08-21', '23:34:39', '', 1),
(1598123594, '2020-08-22', '14:13:14', '', 1),
(1598126393, '2020-08-22', '14:59:53', 'Modificó el estado del :Aspirante :1', 7),
(1598300327, '2020-08-24', '15:18:47', '', 1),
(1598311997, '2020-08-24', '18:33:17', '', 1),
(1598314882, '2020-08-24', '19:21:22', '', 1),
(1598314901, '2020-08-24', '19:21:42', '', 1),
(1598315016, '2020-08-24', '19:23:36', '', 1),
(1598315081, '2020-08-24', '19:24:41', '', 1),
(1598315090, '2020-08-24', '19:24:50', '', 1),
(1598315173, '2020-08-24', '19:26:13', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: /Apellido: /Correo: ', 3),
(1598315185, '2020-08-24', '19:26:25', '', 1),
(1598315258, '2020-08-24', '19:27:38', '', 1),
(1598315265, '2020-08-24', '19:27:45', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Pepepita/Apellido: Garcia/Correo: 2@2.com', 3),
(1598315266, '2020-08-24', '19:27:46', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Pepep/Apellido: Garcia/Correo: 2@2.com', 3),
(1598315275, '2020-08-24', '19:27:55', '', 1),
(1598315357, '2020-08-24', '19:29:17', '', 1),
(1598315632, '2020-08-24', '19:33:52', '', 1),
(1598320022, '2020-08-24', '20:47:02', '', 1),
(1598320035, '2020-08-24', '20:47:15', 'Creacion de proceso cargo: 1598320035', 5),
(1598320075, '2020-08-24', '20:47:55', '', 1),
(1598320083, '2020-08-24', '20:48:03', 'Cargo solicitado: 1598320035', 4),
(1598404508, '2020-08-25', '20:15:08', '', 1),
(1598404558, '2020-08-25', '20:15:58', 'Creacion de proceso cargo: 1598404558', 5),
(1598404624, '2020-08-25', '20:17:04', '', 1),
(1598404674, '2020-08-25', '20:17:54', 'Cargo solicitado: 1598404558', 4),
(1598404750, '2020-08-25', '20:19:10', '', 1),
(1598404781, '2020-08-25', '20:19:41', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Juanit0/Apellido: Alcachofa/Correo: 1@1.com', 3),
(1598404787, '2020-08-25', '20:19:47', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Juanita/Apellido: Alcachofa/Correo: 1@1.com', 3),
(1598404823, '2020-08-25', '20:20:23', '1:27', 6),
(1598404833, '2020-08-25', '20:20:33', '', 1),
(1598404845, '2020-08-25', '20:20:45', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Pepe/Apellido: Garcia/Correo: 2@2.com', 3),
(1598404847, '2020-08-25', '20:20:47', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Pepito/Apellido: Garcia/Correo: 2@2.com', 3),
(1598405777, '2020-08-25', '20:36:17', '', 1),
(1598405789, '2020-08-25', '20:36:29', 'Creacion de proceso cargo: 1598405789', 5),
(1598405831, '2020-08-25', '20:37:11', '', 1),
(1598405864, '2020-08-25', '20:37:44', 'Resolvio encuesta de Espacial y obtuvo un puntaje de 3', 2),
(1598405887, '2020-08-25', '20:38:07', 'Resolvio encuesta de Personalidad y obtuvo un puntaje de 18', 2),
(1598405902, '2020-08-25', '20:38:22', 'Resolvio encuesta de Razonamiento y obtuvo un puntaje de 2', 2),
(1598405960, '2020-08-25', '20:39:20', '', 1),
(1598406034, '2020-08-25', '20:40:34', '', 1),
(1598406046, '2020-08-25', '20:40:46', 'Cargo solicitado: 1598405789', 4),
(1598406066, '2020-08-25', '20:41:06', '', 1),
(1598406157, '2020-08-25', '20:42:37', '', 1),
(1598406162, '2020-08-25', '20:42:43', 'Cargo solicitado: 1598405789', 4),
(1598406170, '2020-08-25', '20:42:50', '', 1),
(1598411150, '2020-08-25', '22:05:50', '', 1),
(1598411182, '2020-08-25', '22:06:22', 'Creacion de proceso cargo: 1598411182', 5),
(1598411201, '2020-08-25', '22:06:41', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Paula/Apellido: Guzman/Correo: 123@123.com', 3),
(1598411206, '2020-08-25', '22:06:46', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Paula/Apellido: Guzman/Correo: 123@123.com', 3),
(1598411218, '2020-08-25', '22:06:58', '', 1),
(1598411247, '2020-08-25', '22:07:27', '', 1),
(1598411273, '2020-08-25', '22:07:53', '', 1),
(1598412714, '2020-08-25', '22:31:54', '', 1),
(1598412989, '2020-08-25', '22:36:29', 'Creacion de proceso cargo: 1598412989', 5),
(1598413329, '2020-08-25', '22:42:09', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Paula/Apellido: Guzman/Correo: 123@123.com', 3),
(1598413334, '2020-08-25', '22:42:14', 'Editó el apartado apellido. Informacion antes de la edición=Nombre: Paulaañlwdas/Apellido: Guzman/Correo: 123@123.com', 3),
(1598413340, '2020-08-25', '22:42:20', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Paulaañlwdas/Apellido: Guzman/Correo: 123@123.com', 3),
(1598413343, '2020-08-25', '22:42:23', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Paula/Apellido: Guzman/Correo: 123@123.com', 3),
(1598413346, '2020-08-25', '22:42:26', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Paulaed/Apellido: Guzman/Correo: 123@123.com', 3),
(1598413377, '2020-08-25', '22:42:57', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Paula/Apellido: Guzman/Correo: 123@123.com', 3),
(1598413379, '2020-08-25', '22:42:59', 'Editó el apartado nombre. Informacion antes de la edición=Nombre: Pauladasa/Apellido: Guzman/Correo: 123@123.com', 3),
(1598413403, '2020-08-25', '22:43:23', '', 1),
(1598414084, '2020-08-25', '22:54:44', 'Cargo solicitado: 1598412989', 4),
(1598414125, '2020-08-25', '22:55:25', '', 1),
(1598414151, '2020-08-25', '22:55:51', '7:25', 6),
(1598414162, '2020-08-25', '22:56:02', '', 1),
(1598414461, '2020-08-25', '23:01:01', '', 1),
(1598415748, '2020-08-25', '23:22:28', '', 1),
(1598415913, '2020-08-25', '23:25:13', 'Modificó el estado del :Aspirante :0', 7),
(1598415933, '2020-08-25', '23:25:33', 'Modificó el estado del :Aspirante :1', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logaccion`
--

CREATE TABLE `logaccion` (
  `idLogAccion` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `logaccion`
--

INSERT INTO `logaccion` (`idLogAccion`, `Nombre`) VALUES
(1, 'Inicio de sesión'),
(2, 'Solución cuestionario'),
(3, 'Edición de información personal'),
(4, 'Postulación a vacante'),
(5, 'Solicitud nuevo cargo'),
(6, 'Entrevista'),
(7, 'Modificación de estado de usuario');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `paginacionrecursos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `paginacionrecursos` (
`idCargo` int(11)
,`Nombre` varchar(45)
,`Vacantes` varchar(45)
,`Descripcion` varchar(400)
,`Fecha` date
,`Estado` varchar(45)
,`idJefe` int(11)
,`idRecursos` int(11)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recursos`
--

CREATE TABLE `recursos` (
  `idRecursos` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Correo` varchar(45) DEFAULT NULL,
  `Clave` varchar(45) DEFAULT NULL,
  `Foto` varchar(45) DEFAULT NULL,
  `Estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `recursos`
--

INSERT INTO `recursos` (`idRecursos`, `Nombre`, `Apellido`, `Correo`, `Clave`, `Foto`, `Estado`) VALUES
(1, 'Pepito', 'Garcia', '2@2.com', 'c81e728d9d4c2f636f067f89cc14862c', 'img/siempre/predeterminada.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recursoslog`
--

CREATE TABLE `recursoslog` (
  `idRecursos` int(11) NOT NULL,
  `idLog` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `recursoslog`
--

INSERT INTO `recursoslog` (`idRecursos`, `idLog`) VALUES
(1, 1598068885),
(1, 1598070879),
(1, 1598311997),
(1, 1598314901),
(1, 1598315016),
(1, 1598315090),
(1, 1598315173),
(1, 1598315258),
(1, 1598315265),
(1, 1598315266),
(1, 1598404833),
(1, 1598404845),
(1, 1598404847),
(1, 1598405960),
(1, 1598406066),
(1, 1598406170),
(1, 1598414162);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `verenc`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `verenc` (
`idEncuesta` int(11)
,`Nombre` varchar(45)
,`Puntaje` int(11)
,`idAspirante` int(11)
,`idEntrevistador` int(11)
,`idCargo` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `verlog`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `verlog` (
`idLog` int(11)
,`fecha` date
,`Hora` time
,`Datos` varchar(400)
,`idLogAccion` int(11)
,`nombre` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `verlogad`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `verlogad` (
`idLog` int(11)
,`Fecha` date
,`Hora` time
,`Datos` varchar(400)
,`idLogAccion` int(11)
,`Accion` varchar(45)
,`idAdministrador` int(11)
,`Actor` varchar(45)
,`Apellido` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `verlogasp`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `verlogasp` (
`idLog` int(11)
,`Fecha` date
,`Hora` time
,`Datos` varchar(400)
,`idLogAccion` int(11)
,`Accion` varchar(45)
,`idAspirante` int(11)
,`Actor` varchar(45)
,`Apellido` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `verloge`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `verloge` (
`idLog` int(11)
,`Fecha` date
,`Hora` time
,`Datos` varchar(400)
,`idLogAccion` int(11)
,`Accion` varchar(45)
,`idEntrevistador` int(11)
,`Actor` varchar(45)
,`Apellido` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `verlogjefe`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `verlogjefe` (
`idLog` int(11)
,`Fecha` date
,`Hora` time
,`Datos` varchar(400)
,`idLogAccion` int(11)
,`Accion` varchar(45)
,`idJefe` int(11)
,`Actor` varchar(45)
,`Apellido` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `verlogrec`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `verlogrec` (
`idLog` int(11)
,`Fecha` date
,`Hora` time
,`Datos` varchar(400)
,`idLogAccion` int(11)
,`Accion` varchar(45)
,`idRecursos` int(11)
,`Actor` varchar(45)
,`Apellido` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `aspirantesrecursos`
--
DROP TABLE IF EXISTS `aspirantesrecursos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `aspirantesrecursos`  AS  select `a`.`idAspirante` AS `idAspirante`,`a`.`Nombre` AS `Nombre`,`a`.`Apellido` AS `Apellido`,`a`.`Correo` AS `Correo`,`a`.`Foto` AS `Foto`,`a`.`Edad` AS `Edad`,`a`.`Sexo` AS `Sexo`,`a`.`Civil` AS `Civil`,`a`.`Descripcion` AS `Descripcion`,`car`.`idCargo` AS `idCargo` from (`cargoaspirante` `car` join `aspirante` `a` on(`car`.`idAspirante` = `a`.`idAspirante`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `paginacionrecursos`
--
DROP TABLE IF EXISTS `paginacionrecursos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `paginacionrecursos`  AS  select `c`.`idCargo` AS `idCargo`,`car`.`Nombre` AS `Nombre`,`car`.`Vacantes` AS `Vacantes`,`car`.`Descripcion` AS `Descripcion`,`car`.`Fecha` AS `Fecha`,`car`.`Estado` AS `Estado`,`car`.`idJefe` AS `idJefe`,`c`.`idRecursos` AS `idRecursos` from (`contratacion` `c` join `cargo` `car` on(`c`.`idCargo` = `car`.`idCargo`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `verenc`
--
DROP TABLE IF EXISTS `verenc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `verenc`  AS  select `e`.`idEncuesta` AS `idEncuesta`,`e`.`Nombre` AS `Nombre`,`e`.`Puntaje` AS `Puntaje`,`e`.`idAspirante` AS `idAspirante`,`e`.`idEntrevistador` AS `idEntrevistador`,`ca`.`idCargo` AS `idCargo` from ((`encuesta` `e` join `aspirante` `a` on(`a`.`idAspirante` = `e`.`idAspirante`)) join `cargoaspirante` `ca` on(`ca`.`idAspirante` = `e`.`idAspirante`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `verlog`
--
DROP TABLE IF EXISTS `verlog`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `verlog`  AS  select `l`.`idLog` AS `idLog`,`l`.`Fecha` AS `fecha`,`l`.`Hora` AS `Hora`,`l`.`Datos` AS `Datos`,`l`.`idLogAccion` AS `idLogAccion`,`a`.`Nombre` AS `nombre` from (`log` `l` join `logaccion` `a` on(`l`.`idLogAccion` = `a`.`idLogAccion`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `verlogad`
--
DROP TABLE IF EXISTS `verlogad`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `verlogad`  AS  select `l`.`idLog` AS `idLog`,`l`.`Fecha` AS `Fecha`,`l`.`Hora` AS `Hora`,`l`.`Datos` AS `Datos`,`l`.`idLogAccion` AS `idLogAccion`,`ac`.`Nombre` AS `Accion`,`al`.`idAdministrador` AS `idAdministrador`,`a`.`Nombre` AS `Actor`,`a`.`Apellido` AS `Apellido` from (((`log` `l` join `logaccion` `ac` on(`l`.`idLogAccion` = `ac`.`idLogAccion`)) join `administradorlog` `al` on(`al`.`idLog` = `l`.`idLog`)) join `administrador` `a` on(`a`.`idAdministrador` = `al`.`idAdministrador`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `verlogasp`
--
DROP TABLE IF EXISTS `verlogasp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `verlogasp`  AS  select `l`.`idLog` AS `idLog`,`l`.`Fecha` AS `Fecha`,`l`.`Hora` AS `Hora`,`l`.`Datos` AS `Datos`,`l`.`idLogAccion` AS `idLogAccion`,`a`.`Nombre` AS `Accion`,`asp`.`idAspirante` AS `idAspirante`,`ac`.`Nombre` AS `Actor`,`ac`.`Apellido` AS `Apellido` from (((`log` `l` join `logaccion` `a` on(`l`.`idLogAccion` = `a`.`idLogAccion`)) join `aspirantelog` `asp` on(`asp`.`idLog` = `l`.`idLog`)) join `aspirante` `ac` on(`ac`.`idAspirante` = `asp`.`idAspirante`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `verloge`
--
DROP TABLE IF EXISTS `verloge`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `verloge`  AS  select `l`.`idLog` AS `idLog`,`l`.`Fecha` AS `Fecha`,`l`.`Hora` AS `Hora`,`l`.`Datos` AS `Datos`,`l`.`idLogAccion` AS `idLogAccion`,`ac`.`Nombre` AS `Accion`,`el`.`idEntrevistador` AS `idEntrevistador`,`e`.`Nombre` AS `Actor`,`e`.`Apellido` AS `Apellido` from (((`log` `l` join `logaccion` `ac` on(`l`.`idLogAccion` = `ac`.`idLogAccion`)) join `entrevistadorlog` `el` on(`el`.`idLog` = `l`.`idLog`)) join `entrevistador` `e` on(`e`.`idEntrevistador` = `el`.`idEntrevistador`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `verlogjefe`
--
DROP TABLE IF EXISTS `verlogjefe`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `verlogjefe`  AS  select `l`.`idLog` AS `idLog`,`l`.`Fecha` AS `Fecha`,`l`.`Hora` AS `Hora`,`l`.`Datos` AS `Datos`,`l`.`idLogAccion` AS `idLogAccion`,`ac`.`Nombre` AS `Accion`,`jl`.`idJefe` AS `idJefe`,`j`.`Nombre` AS `Actor`,`j`.`Apellido` AS `Apellido` from (((`log` `l` join `logaccion` `ac` on(`l`.`idLogAccion` = `ac`.`idLogAccion`)) join `jefelog` `jl` on(`jl`.`idLog` = `l`.`idLog`)) join `jefe` `j` on(`j`.`idJefe` = `jl`.`idJefe`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `verlogrec`
--
DROP TABLE IF EXISTS `verlogrec`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `verlogrec`  AS  select `l`.`idLog` AS `idLog`,`l`.`Fecha` AS `Fecha`,`l`.`Hora` AS `Hora`,`l`.`Datos` AS `Datos`,`l`.`idLogAccion` AS `idLogAccion`,`ac`.`Nombre` AS `Accion`,`rl`.`idRecursos` AS `idRecursos`,`r`.`Nombre` AS `Actor`,`r`.`Apellido` AS `Apellido` from (((`log` `l` join `logaccion` `ac` on(`l`.`idLogAccion` = `ac`.`idLogAccion`)) join `recursoslog` `rl` on(`rl`.`idLog` = `l`.`idLog`)) join `recursos` `r` on(`r`.`idRecursos` = `rl`.`idRecursos`)) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`idActividad`,`idContratacion`),
  ADD KEY `fk_Actividad_Contratacion1_idx` (`idContratacion`);

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `administradorlog`
--
ALTER TABLE `administradorlog`
  ADD PRIMARY KEY (`idAdministrador`,`idLog`),
  ADD KEY `fk_Administrador_has_Log_Log1_idx` (`idLog`),
  ADD KEY `fk_Administrador_has_Log_Administrador1_idx` (`idAdministrador`);

--
-- Indices de la tabla `aspirante`
--
ALTER TABLE `aspirante`
  ADD PRIMARY KEY (`idAspirante`);

--
-- Indices de la tabla `aspirantelog`
--
ALTER TABLE `aspirantelog`
  ADD PRIMARY KEY (`idAspirante`,`idLog`),
  ADD KEY `fk_Aspirante_has_Log_Log1_idx` (`idLog`),
  ADD KEY `fk_Aspirante_has_Log_Aspirante1_idx` (`idAspirante`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`idCargo`,`idJefe`),
  ADD KEY `fk_Cargo_Jefe1_idx` (`idJefe`);

--
-- Indices de la tabla `cargoaspirante`
--
ALTER TABLE `cargoaspirante`
  ADD PRIMARY KEY (`idCargoAspirante`,`idCargo`,`idAspirante`),
  ADD KEY `fk_Cargo_has_Aspirante_Aspirante1_idx` (`idAspirante`),
  ADD KEY `fk_Cargo_has_Aspirante_Cargo_idx` (`idCargo`);

--
-- Indices de la tabla `contratacion`
--
ALTER TABLE `contratacion`
  ADD PRIMARY KEY (`idContratacion`,`idRecursos`,`idCargo`),
  ADD KEY `fk_Contratacion_Recursos1_idx` (`idRecursos`),
  ADD KEY `fk_Contratacion_Cargo1_idx` (`idCargo`);

--
-- Indices de la tabla `encuesta`
--
ALTER TABLE `encuesta`
  ADD PRIMARY KEY (`idEncuesta`,`idAspirante`,`idEntrevistador`),
  ADD KEY `fk_Encuesta_Aspirante1_idx` (`idAspirante`),
  ADD KEY `fk_Encuesta_Entrevistador1_idx` (`idEntrevistador`);

--
-- Indices de la tabla `entrevistador`
--
ALTER TABLE `entrevistador`
  ADD PRIMARY KEY (`idEntrevistador`);

--
-- Indices de la tabla `entrevistadorlog`
--
ALTER TABLE `entrevistadorlog`
  ADD PRIMARY KEY (`idEntrevistador`,`idLog`),
  ADD KEY `fk_Entrevistador_has_Log_Log1_idx` (`idLog`),
  ADD KEY `fk_Entrevistador_has_Log_Entrevistador1_idx` (`idEntrevistador`);

--
-- Indices de la tabla `jefe`
--
ALTER TABLE `jefe`
  ADD PRIMARY KEY (`idJefe`);

--
-- Indices de la tabla `jefelog`
--
ALTER TABLE `jefelog`
  ADD PRIMARY KEY (`idJefe`,`idLog`),
  ADD KEY `fk_Jefe_has_Log_Log1_idx` (`idLog`),
  ADD KEY `fk_Jefe_has_Log_Jefe1_idx` (`idJefe`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`idLog`,`idLogAccion`),
  ADD KEY `fk_Log_LogAccion1_idx` (`idLogAccion`);

--
-- Indices de la tabla `logaccion`
--
ALTER TABLE `logaccion`
  ADD PRIMARY KEY (`idLogAccion`);

--
-- Indices de la tabla `recursos`
--
ALTER TABLE `recursos`
  ADD PRIMARY KEY (`idRecursos`);

--
-- Indices de la tabla `recursoslog`
--
ALTER TABLE `recursoslog`
  ADD PRIMARY KEY (`idRecursos`,`idLog`),
  ADD KEY `fk_Recursos_has_Log_Log1_idx` (`idLog`),
  ADD KEY `fk_Recursos_has_Log_Recursos1_idx` (`idRecursos`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `idActividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `aspirante`
--
ALTER TABLE `aspirante`
  MODIFY `idAspirante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `idCargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1598412990;

--
-- AUTO_INCREMENT de la tabla `cargoaspirante`
--
ALTER TABLE `cargoaspirante`
  MODIFY `idCargoAspirante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `contratacion`
--
ALTER TABLE `contratacion`
  MODIFY `idContratacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `encuesta`
--
ALTER TABLE `encuesta`
  MODIFY `idEncuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1598414152;

--
-- AUTO_INCREMENT de la tabla `entrevistador`
--
ALTER TABLE `entrevistador`
  MODIFY `idEntrevistador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `jefe`
--
ALTER TABLE `jefe`
  MODIFY `idJefe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `logaccion`
--
ALTER TABLE `logaccion`
  MODIFY `idLogAccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `recursos`
--
ALTER TABLE `recursos`
  MODIFY `idRecursos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `fk_Actividad_Contratacion1` FOREIGN KEY (`idContratacion`) REFERENCES `contratacion` (`idContratacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `administradorlog`
--
ALTER TABLE `administradorlog`
  ADD CONSTRAINT `fk_Administrador_has_Log_Administrador1` FOREIGN KEY (`idAdministrador`) REFERENCES `administrador` (`idAdministrador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Administrador_has_Log_Log1` FOREIGN KEY (`idLog`) REFERENCES `log` (`idLog`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `aspirantelog`
--
ALTER TABLE `aspirantelog`
  ADD CONSTRAINT `fk_Aspirante_has_Log_Aspirante1` FOREIGN KEY (`idAspirante`) REFERENCES `aspirante` (`idAspirante`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Aspirante_has_Log_Log1` FOREIGN KEY (`idLog`) REFERENCES `log` (`idLog`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD CONSTRAINT `fk_Cargo_Jefe1` FOREIGN KEY (`idJefe`) REFERENCES `jefe` (`idJefe`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cargoaspirante`
--
ALTER TABLE `cargoaspirante`
  ADD CONSTRAINT `fk_Cargo_has_Aspirante_Aspirante1` FOREIGN KEY (`idAspirante`) REFERENCES `aspirante` (`idAspirante`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Cargo_has_Aspirante_Cargo` FOREIGN KEY (`idCargo`) REFERENCES `cargo` (`idCargo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contratacion`
--
ALTER TABLE `contratacion`
  ADD CONSTRAINT `fk_Contratacion_Cargo1` FOREIGN KEY (`idCargo`) REFERENCES `cargo` (`idCargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratacion_Recursos1` FOREIGN KEY (`idRecursos`) REFERENCES `recursos` (`idRecursos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `encuesta`
--
ALTER TABLE `encuesta`
  ADD CONSTRAINT `fk_Encuesta_Aspirante1` FOREIGN KEY (`idAspirante`) REFERENCES `aspirante` (`idAspirante`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Encuesta_Entrevistador1` FOREIGN KEY (`idEntrevistador`) REFERENCES `entrevistador` (`idEntrevistador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `entrevistadorlog`
--
ALTER TABLE `entrevistadorlog`
  ADD CONSTRAINT `fk_Entrevistador_has_Log_Entrevistador1` FOREIGN KEY (`idEntrevistador`) REFERENCES `entrevistador` (`idEntrevistador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Entrevistador_has_Log_Log1` FOREIGN KEY (`idLog`) REFERENCES `log` (`idLog`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `jefelog`
--
ALTER TABLE `jefelog`
  ADD CONSTRAINT `fk_Jefe_has_Log_Jefe1` FOREIGN KEY (`idJefe`) REFERENCES `jefe` (`idJefe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Jefe_has_Log_Log1` FOREIGN KEY (`idLog`) REFERENCES `log` (`idLog`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `fk_Log_LogAccion1` FOREIGN KEY (`idLogAccion`) REFERENCES `logaccion` (`idLogAccion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recursoslog`
--
ALTER TABLE `recursoslog`
  ADD CONSTRAINT `fk_Recursos_has_Log_Log1` FOREIGN KEY (`idLog`) REFERENCES `log` (`idLog`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Recursos_has_Log_Recursos1` FOREIGN KEY (`idRecursos`) REFERENCES `recursos` (`idRecursos`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
